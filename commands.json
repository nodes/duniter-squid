{
  "$schema": "https://cdn.subsquid.io/schemas/commands.json",
  "commands": {
    "download-genesis": {
      "description": "Download genesis data from GitLab artifact or other source.",
      "cmd": ["./scripts/download_genesis.sh"]
    },
    "clean": {
      "description": "delete all build artifacts",
      "cmd": ["npx", "--yes", "rimraf", "lib"]
    },
    "build": {
      "description": "Build the squid project",
      "deps": ["clean"],
      "cmd": ["npm", "run", "build"]
    },
    "up": {
      "description": "Start a PG database and Hasura GraphQL engine",
      "deps": ["up-only", "migration:apply", "hasura:apply"],
      "cmd": ["echo", "Database and Hasura have been started"]
    },
    "up-only": {
      "description": "Start a PG database and Hasura GraphQL engine",
      "cmd": ["docker", "compose", "-f", "docker-compose.dev.yml", "up", "-d"]
    },
    "down": {
      "description": "Drop a PG database and Hasura GraphQL engine",
      "cmd": ["docker", "compose", "-f", "docker-compose.dev.yml", "down"]
    },
    "migration:copy-custom": {
      "description": "Copy custom migrations to the migrations folder",
      "cmd": ["./scripts/add_custom_squid_migrations.sh"],
      "hidden": true
    },
    "migration:apply": {
      "description": "Apply the DB migrations",
      "deps": ["migration:copy-custom"],
      "cmd": ["squid-typeorm-migration", "apply"]
    },
    "migration:generate": {
      "description": "Generate a DB migration matching the TypeORM entities",
      "deps": ["build", "migration:clean"],
      "cmd": ["squid-typeorm-migration", "generate"]
    },
    "migration:clean": {
      "description": "Clean the migrations folder",
      "cmd": ["npx", "--yes", "rimraf", "./db/migrations"]
    },
    "migration": {
      "deps": ["build"],
      "cmd": ["squid-typeorm-migration", "generate"],
      "hidden": true
    },
    "db:update": {
      "description": "Apply database changes: codegen, generate and apply migrations, clean migrations, track tables and functions",
      "deps": [
        "down",
        "up-only",
        "codegen",
        "migration:generate",
        "migration:apply",
        "hasura:generate",
        "hasura:apply"
      ],
      "cmd": ["echo", "Database and Hasura metadata updated"]
    },
    "hasura:apply": {
      "description": "Apply Hasura metadata",
      "cmd": ["hasura", "metadata", "apply"]
    },
    "hasura:generate": {
      "description": "Generate Hasura metadata",
      "cmd": ["node", "lib/generateHasuraMetadata.js"]
    },
    "hasura:update": {
      "description": "Generate Hasura fresh metadata from new script",
      "deps": ["build", "hasura:generate", "hasura:apply"],
      "cmd": ["echo", "Hasura metadata updated"]
    },
    "codegen": {
      "description": "Generate TypeORM entities from the schema file",
      "cmd": ["squid-typeorm-codegen"]
    },
    "typegen": {
      "description": "Generate data access classes for an substrate metadata",
      "cmd": ["squid-substrate-typegen", "./typegen.json"]
    },
    "serve": {
      "description": "Start the GraphQL API server",
      "cmd": ["squid-graphql-server", "--subscriptions"]
    },
    "serve:prod": {
      "description": "Start the GraphQL API server with caching and limits",
      "cmd": [
        "squid-graphql-server",
        "--dumb-cache",
        "in-memory",
        "--dumb-cache-ttl",
        "1000",
        "--dumb-cache-size",
        "100",
        "--dumb-cache-max-age",
        "1000",
        "--subscriptions"
      ]
    },
    "process": {
      "description": "Load .env and start the squid processor",
      "cmd": [
        "node",
        "--require=dotenv/config",
        "-max-old-space-size=4096",
        "lib/main.js"
      ]
    },
    "process:prod": {
      "description": "Start the squid processor",
      "deps": ["migration:apply"],
      "cmd": ["node", "-max-old-space-size=4096", "lib/main.js"],
      "hidden": true
    },
    "check-updates": {
      "cmd": [
        "npx",
        "--yes",
        "npm-check-updates",
        "--filter=/subsquid/",
        "--upgrade"
      ],
      "hidden": true
    },
    "bump": {
      "description": "Bump @subsquid packages to the latest versions",
      "deps": ["check-updates"],
      "cmd": ["npm", "i", "-f"]
    },
    "open": {
      "description": "Open a local browser window",
      "cmd": ["npx", "--yes", "opener"]
    }
  }
}
