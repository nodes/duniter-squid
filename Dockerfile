# --- node 20 base image
FROM node:20-alpine AS node
ENV PNPM_HOME="/pnpm"
ENV PATH="$PNPM_HOME:$PATH"
RUN corepack enable

# Add build dependencies to build `pnpm` which does not have pre-built packages for ARM64
# RUN apk add --no-cache python3 make gcc g++

# --- prod dependencies only
FROM node AS deps
WORKDIR /squid
COPY package.json pnpm-lock.yaml .
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --prod

# --- dev dependencies to build
FROM deps AS builder
WORKDIR /squid
COPY tsconfig.json .
COPY src src
RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install
RUN pnpm build

# # --- use this if you want to download when building docker image
# FROM curlimages/curl AS input
# WORKDIR /squid
# COPY /scripts/ scripts/
# RUN mkdir ./input
# RUN ./scripts/download_genesis.sh

# --- use this if you want to copy from pre-downloaded files
FROM node AS input
WORKDIR /squid
COPY /input/ input/

# --- final squid image
FROM node AS squid
WORKDIR /squid
# copy genesis and history (only change on reboot)
COPY --from=input /squid/input input
# copy assets
COPY assets assets 
COPY db db
COPY schema.graphql .
# copy prod deps only
COPY --from=deps /squid/node_modules node_modules
# copy build
COPY --from=builder /squid/lib lib
# copy scripts
COPY /scripts/ scripts/
COPY commands.json .
# it seems not to work with pnpm installed version. `pnpm exec squid-commands` does not work
RUN npm i -g @subsquid/commands && mv $(which squid-commands) /usr/local/bin/sqd

# environment variables
# ENV PROCESSOR_PROMETHEUS_PORT 3000
ENV GENESIS_FILE="./input/gdev.json"
ENV HIST_GEN_FILE="./input/genesis.json"
ENV HIST_BLOCK_FILE="./input/block_hist.json"
ENV HIST_TX_FILE="./input/tx_hist.json"
ENV HIST_CERT_FILE="./input/cert_hist.json"

# ENTRYPOINT ["pnpm", "exec", "squid-commands", "process:prod"]
