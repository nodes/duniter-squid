const fs = require("fs");

module.exports = class _sqlReqName__timestamp_ {
  name = "_sqlReqName__timestamp_";

  async up(db) {
    await db.query(fs.readFileSync("assets/sql/_sqlReqName__up.sql", "utf8"));
  }

  async down(db) {
    await db.query(fs.readFileSync("assets/sql/_sqlReqName__down.sql", "utf8"));
  }
};
