DROP TABLE IF EXISTS item_type;
DROP TABLE IF EXISTS counter_level;
DROP TABLE IF EXISTS smith_status;
DROP TABLE IF EXISTS identity_status;
DROP TABLE IF EXISTS event_type;
DROP TABLE IF EXISTS smith_event_type;
DROP TABLE IF EXISTS comment_type;
