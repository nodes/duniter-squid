const fs = require("fs");

module.exports = class udHistoryFunction1730814191993 {
  name = "udHistoryFunction1730814191993";

  async up(db) {
    await db.query(fs.readFileSync("assets/sql/udHistoryFunction_up.sql", "utf8"));
  }

  async down(db) {
    await db.query(fs.readFileSync("assets/sql/udHistoryFunction_down.sql", "utf8"));
  }
};
