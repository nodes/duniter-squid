const fs = require("fs");

module.exports = class EnumsMigration1730814191993 {
  name = "EnumsMigration1730814191993";

  async up(db) {
    await db.query(fs.readFileSync("assets/sql/EnumsMigration_up.sql", "utf8"));
  }

  async down(db) {
    await db.query(fs.readFileSync("assets/sql/EnumsMigration_down.sql", "utf8"));
  }
};
