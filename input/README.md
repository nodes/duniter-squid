Input folder gets input data from remote source. Its content is not commited.

Get data with

```sh
sqd download-genesis
```

You can use `/input/fake/*` directory for light empty history.

For new `gdev.json`, do:

```sh
duniter build-spec --chain=dev > ./input/fake/gdev.json
```