#!/bin/sh

# Get custom migrations names from the assets/sql directory
for file in assets/sql/*_up.sql; do
    # Get the basename of file and strips the '_up.sql' suffix
    migration_name=$(basename "$file" "_up.sql")
    # migration_name=$(basename "$file" "_up.sql") is not working with sh, so we do this shit:
    migrations_names="$migrations_names $migration_name"
done

migrations_dir="db/migrations"
template_migration_file="assets/sql/squidMigration.js"
new_timestamp=$(($(date +%s%N) / 1000000 + 120000))

for migration_name in $migrations_names; do
    new_migration_file="$migrations_dir/$new_timestamp-$migration_name.js"

    if [ -d "$migrations_dir" ] && [ "$(ls -A $migrations_dir)" ]; then
        # Check if the specific file does not exist and copy it if necessary
        if ! ls $migrations_dir/*$migration_name.js 1> /dev/null 2>&1; then
            # Copy the file to the migrations directory
            cp "$template_migration_file" "$new_migration_file"
            # Replace name and timestamp placeholders
            sed -i "s/_sqlReqName_/$migration_name/g" $new_migration_file
            sed -i "s/_timestamp_/$new_timestamp/g" $new_migration_file
        fi
    fi
done