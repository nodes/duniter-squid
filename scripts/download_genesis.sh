#!/bin/sh

# can be called with `sqd download-genesis`
JOB=131675 # id of the create_g1_data job

# download output from py-g1-migrator CI run by Duniter
# genesis.json is only used for export block
wget https://git.duniter.org/nodes/rust/duniter-v2s/-/jobs/$JOB/artifacts/download -O artifacts.zip
unzip -j artifacts.zip -d input
rm artifacts.zip

# download gdev.json
# for the moment, use previous version commited to duniter-squid since Duniter artifacts expired
# when new artifacts are generated and set to never expire, will use it instead
wget "https://git.duniter.org/nodes/duniter-squid/-/raw/add-certifications-date/assets/gdev.json?ref_type=heads" -O ./input/gdev.json
