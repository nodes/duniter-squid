import { CID } from 'multiformats'

const cid1 = CID.parse("QmPRaxu3YGuqfJGbvq2MqqXkEXmMAmHG8aioXJGGSQk3Vv")
const cid2 = CID.parse("bafybeiaqd5nujg6c2bckoj26sl6fxpbf7ak2zw7zmqgjui5bvp7wflrw3u")

console.log("0x" + Buffer.from(cid1.bytes).toString("hex"));
console.log("0x" + Buffer.from(cid2.bytes).toString("hex"));
