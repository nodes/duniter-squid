#!/bin/sh

# Start hasura in background (with naming conventions to allow camelCase defined in databases.yaml)
HASURA_GRAPHQL_EXPERIMENTAL_FEATURES=naming_convention graphql-engine serve &

endpoint="http://localhost:8080"

check_hasura_ready() {
  response=$(curl -s -o /dev/null -w "%{http_code}" $endpoint/healthz)
  echo $response
}

echo "Waiting for hasura..."
while [ $(check_hasura_ready) -ne 200 ]; do
  sleep 1
done
echo "Hasura is ready."

# if [ "$PRODUCTION" = "true" ]; then
#   sed -i 's/host.docker.internal:3000/squid-app:3000/g' hasura/metadata/actions.yaml
# else
#   sed -i 's/squid-app:3000/host.docker.internal:3000/g' hasura/metadata/actions.yaml
# fi

# Apply Hasura metadata
hasura metadata apply

trap : SIGINT; sleep infinity & wait
