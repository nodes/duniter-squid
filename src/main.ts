import { TypeormDatabaseWithCache } from "@belopash/typeorm-store";
import { strict as assert } from 'assert';
import { DataHandler } from "./data_handler";
import { saveGenesis } from "./genesis/genesis";
import { saveBlock, saveCall, saveEvent, saveExtrinsic } from "./giant-squid";
import { processor } from "./processor";
import { constants, events as events_t } from "./types";
import { Ctx, NewData, AccPopulationHistory } from "./types_custom";
import { ss58encode } from "./utils";

// main processor loop able to manage a batch of blocks
processor.run(new TypeormDatabaseWithCache(), async (ctx) => {
  // this part is adapted from giant squid
  // https://github.com/subsquid-labs/giant-squid-explorer/blob/main/src/main.ts
  for (const { header, calls, events, extrinsics } of ctx.blocks) {
    ctx.log.debug(
      `block ${header.height}: extrinsics - ${extrinsics.length}, calls - ${calls.length}, events - ${events.length}`
    );
    // save the block header
    const genesisBlock = await saveBlock(ctx, header);
    // manage genesis state
    if (header.height === 0) {
      await saveGenesis(ctx, genesisBlock);
    }
    // save all extrinsics of the block
    for (const extrinsic of extrinsics) {
      await saveExtrinsic(ctx, extrinsic);
    }
    // save all calls of the block
    for (const call of calls.reverse()) {
      await saveCall(ctx, call);
    }
    // save all events of the block
    for (const event of events) {
      await saveEvent(ctx, event);
    }
  }

  const newData: NewData = {
    accounts: [],
    killedAccounts: [],
    validators: [],
    populationHistories: [],
    identitiesCreated: [],
    identitiesConfirmed: [],
    identitiesRemoved: [],
    identitiesRevoked: [],
    idtyChangedOwnerKey: [],
    membershipAdded: [],
    membershipRemoved: [],
    membershipRenewed: [],
    transfers: [],
    certCreation: [],
    certRenewal: [],
    certRemoval: [],
    accountLink: [],
    accountUnlink: [],
    smithCertAdded: [],
    smithCertRemoved: [],
    smithExcluded: [],
    smithPromoted: [],
    smithInvited: [],
    smithAccepted: [],
    universalDividend: [],
    udReeval: [],
    comments: []
  };
  collectDataFromEvents(ctx, newData);

  const dataHandler = new DataHandler();
  await dataHandler.processNewData(newData, ctx);

  // HACK to handle circular dependency, we are forced to create accounts+ identities
  // first and commit the changes before updating the rest
  await dataHandler.handleNewAccountsApart(ctx, newData);
  await dataHandler.handleNewIdentitiesApart(ctx, newData);

  // store data
  await dataHandler.storeData(ctx);
});

function collectDataFromEvents(ctx: Ctx, newData: NewData) {
  const silence_events = [
    events_t.system.extrinsicSuccess.name,
    events_t.certification.certRemoved.name,
    events_t.session.newSession.name,
    events_t.imOnline.allGood.name,
    events_t.membership.membershipRemoved.name,
    events_t.universalDividend.udsAutoPaid.name,
    events_t.universalDividend.newUdCreated.name,
    events_t.balances.withdraw.name,
    events_t.balances.deposit.name,
    events_t.transactionPayment.transactionFeePaid.name,
    events_t.identity.idtyRemoved.name,
    events_t.quota.refunded.name,
  ];

  // accumulator to count population history
  const acc: AccPopulationHistory = { activeAccountCount: 0, memberCount: 0, smithCount: 0, blockNumber: -1 };

  ctx.blocks.forEach((block) => {

    const validator = block.header.validator;
    if (validator != null) {
      // (only block 0 has null validator)
      const blockNumber = block.header.height;
      newData.validators.push({ block: blockNumber, validatorId: ss58encode(validator) });
    }

    block.events.forEach((event) => {
      if (!silence_events.includes(event.name)) {
        ctx.log.info("" + block.header.height + " " + event.name);
      }
      switch (event.name) {
        case events_t.system.newAccount.name: {
          const evt = events_t.system.newAccount.v800.decode(event);
          newData.accounts.push({
            address: ss58encode(evt.account),
            blockNumber: block.header.height
          });
          acc.activeAccountCount += 1;
          acc.blockNumber = block.header.height;
          break;
        }

        case events_t.system.killedAccount.name: {
          const evt = events_t.system.killedAccount.v800.decode(event);
          newData.killedAccounts.push(ss58encode(evt.account));
          acc.activeAccountCount -= 1;
          break;
        }

        // commented calls
        case events_t.system.remarked.name: {
          const evt = events_t.system.remarked.v800.decode(event);
          newData.comments.push({ event, hash: evt.hash, sender: ss58encode(evt.sender), blockNumber: block.header.height })
          break;
        }

        case events_t.account.accountLinked.name: {
          const evt =
            events_t.account.accountLinked.v800.decode(event);
          newData.accountLink.push({
            accountId: ss58encode(evt.who),
            index: evt.identity,
            event: event,
          });
          break;
        }

        case events_t.account.accountUnlinked.name: {
          const evt =
            events_t.account.accountUnlinked.v800.decode(event);
          newData.accountUnlink.push({
            accountId: ss58encode(evt),
            event: event,
          });
          break;
        }

        case events_t.balances.transfer.name: {
          const evt = events_t.balances.transfer.v800.decode(event);
          assert(
            block.header.timestamp,
            `Got an undefined timestamp at block ${block.header.height}`
          );
          newData.transfers.push({
            id: event.id,
            blockNumber: block.header.height,
            timestamp: new Date(block.header.timestamp),
            from: ss58encode(evt.from),
            to: ss58encode(evt.to),
            amount: evt.amount,
            event: event,
          });
          break;
        }

        case events_t.identity.idtyCreated.name: {
          const evt = events_t.identity.idtyCreated.v800.decode(event);
          newData.identitiesCreated.push({
            index: evt.idtyIndex,
            accountId: ss58encode(evt.ownerKey),
            blockNumber: block.header.height,
            // after identity creation, there is a given period to confirm
            expireOn:
              block.header.height +
              constants.identity.confirmPeriod.v800.get(event.block),
            // confirm period is time available for confirm
            // TODO add this to the Duniter event
            event: event,
          });
          break;
        }

        case events_t.identity.idtyConfirmed.name: {
          const evt =
            events_t.identity.idtyConfirmed.v800.decode(event);
          newData.identitiesConfirmed.push({
            id: event.id,
            index: evt.idtyIndex,
            name: evt.name,
            blockNumber: block.header.height,
            // after identity confirmation, there is a given period to validate
            expireOn:
              block.header.height +
              constants.identity.validationPeriod.v800.get(event.block),
            // validation period is time available for validation
            // TODO add this to the Duniter event
            event: event,
          });
          break;
        }

        // Note: removed since it is handled by membership event
        // case events_t.identity.idtyValidated.name: {
        //   const evt =
        //     events_t.identity.idtyValidated.v800.decode(event);
        //   newData.identitiesValidated.push({
        //     id: event.id,
        //     index: evt.idtyIndex,
        //     blockNumber: block.header.height,
        //     // after identity validation, there is a given period to renew membership
        //     // expireOn:
        //     //   block.header.height +
        //     //   constants.membership.membershipPeriod.v800.get(event.block),
        //     // membership period is time where membership is valid
        //     event: event,
        //   });
        //   break;
        // }

        case events_t.identity.idtyRemoved.name: {
          const evt = events_t.identity.idtyRemoved.v800.decode(event);
          newData.identitiesRemoved.push({
            index: evt.idtyIndex,
            reason: evt.reason,
          });
          break;
        }

        case events_t.identity.idtyRevoked.name: {
          const evt = events_t.identity.idtyRevoked.v800.decode(event);
          newData.identitiesRevoked.push({
            id: event.id,
            index: evt.idtyIndex,
            reason: evt.reason,
            blockNumber: block.header.height,
            // when identity is revoked, we record the block
            expireOn: block.header.height,
            event: event,
          });
          break;
        }

        case events_t.identity.idtyChangedOwnerKey.name: {
          const evt =
            events_t.identity.idtyChangedOwnerKey.v800.decode(event);
          newData.idtyChangedOwnerKey.push({
            id: event.id,
            index: evt.idtyIndex,
            accountId: ss58encode(evt.newOwnerKey),
            blockNumber: block.header.height,
            event: event,
          });
          break;
        }

        case events_t.membership.membershipAdded.name: {
          const evt =
            events_t.membership.membershipAdded.v800.decode(event);
          newData.membershipAdded.push({
            index: evt.member,
            // after membership added, there is a given time to renew
            expireOn: evt.expireOn,
            // should be equal to:
            //   block.header.height +
            //   constants.membership.membershipPeriod.v800.get(event.block),
            event: event,
          });
          acc.memberCount += 1;
          acc.blockNumber = block.header.height;
          break;
        }

        case events_t.membership.membershipRemoved.name: {
          const evt =
            events_t.membership.membershipRemoved.v800.decode(event);
          newData.membershipRemoved.push({
            index: evt.member,
            reason: evt.reason,
            // after membership is removed, there is a given time to re-join, before autorevocation
            expireOn: block.header.height + constants.identity.autorevocationPeriod.v800.get(event.block),
            event: event,
          });
          acc.memberCount -= 1;
          acc.blockNumber = block.header.height;
          break;
        }

        case events_t.membership.membershipRenewed.name: {
          const evt =
            events_t.membership.membershipRenewed.v800.decode(event);
          newData.membershipRenewed.push({
            index: evt.member,
            // after membership renewed, there is a given time to renew again
            expireOn: evt.expireOn,
            // should be equal to:
            //   block.header.height +
            //   constants.membership.membershipPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.certification.certAdded.name: {
          const evt = events_t.certification.certAdded.v800.decode(event);
          newData.certCreation.push({
            issuerId: evt.issuer,
            receiverId: evt.receiver,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.certification.validityPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.certification.certRenewed.name: {
          const evt =
            events_t.certification.certRenewed.v800.decode(event);
          newData.certRenewal.push({
            issuerId: evt.issuer,
            receiverId: evt.receiver,
            blockNumber: block.header.height,
            expireOn:
              block.header.height +
              constants.certification.validityPeriod.v800.get(event.block),
            event: event,
          });
          break;
        }

        case events_t.certification.certRemoved.name: {
          const evt =
            events_t.certification.certRemoved.v800.decode(event);
          newData.certRemoval.push({
            issuerId: evt.issuer,
            receiverId: evt.receiver,
            blockNumber: block.header.height,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.smithCertAdded.name: {
          const evt =
            events_t.smithMembers.smithCertAdded.v800.decode(event);
          newData.smithCertAdded.push({
            id: event.id,
            issuerId: evt.issuer,
            receiverId: evt.receiver,
            createdOn: block.header.height,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.smithCertRemoved.name: {
          const evt =
            events_t.smithMembers.smithCertRemoved.v800.decode(event);
          newData.smithCertRemoved.push({
            id: event.id,
            issuerId: evt.issuer,
            receiverId: evt.receiver,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.smithMembershipAdded.name: {
          const evt =
            events_t.smithMembers.smithMembershipAdded.v800.decode(event);
          newData.smithPromoted.push({
            id: event.id,
            idtyIndex: evt.idtyIndex,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.smithMembershipRemoved.name: {
          const evt =
            events_t.smithMembers.smithMembershipRemoved.v800.decode(event);
          newData.smithExcluded.push({
            id: event.id,
            idtyIndex: evt.idtyIndex,
            event: event,
          });
          acc.smithCount += 1;
          acc.blockNumber = block.header.height;
          break;
        }

        case events_t.smithMembers.invitationSent.name: {
          const evt =
            events_t.smithMembers.invitationSent.v800.decode(event);
          newData.smithInvited.push({
            id: event.id,
            idtyIndex: evt.receiver,
            invitedBy: evt.issuer,
            event: event,
          });
          break;
        }

        case events_t.smithMembers.invitationAccepted.name: {
          const evt =
            events_t.smithMembers.invitationAccepted.v800.decode(event);
          newData.smithAccepted.push({
            id: event.id,
            idtyIndex: evt.idtyIndex,
            event: event,
          });
          acc.smithCount -= 1;
          acc.blockNumber = block.header.height;
          break;
        }

        case events_t.universalDividend.newUdCreated.name: {
          const evt = events_t.universalDividend.newUdCreated.v800.decode(event);
          ctx.log.info(`New UD created at block ${block.header.height} with amount ${evt.amount}`);
          newData.universalDividend.push({
            blockNumber: block.header.height,
            timestamp: new Date(block.header.timestamp!),
            amount: evt.amount,
            monetaryMass: evt.monetaryMass,
            membersCount: evt.membersCount,
            event: event,
          });
          break;
        }

        case events_t.universalDividend.udReevalued.name: {
          const evt = events_t.universalDividend.udReevalued.v800.decode(event);
          ctx.log.info(`UD has been reevaluated at block ${block.header.height} with new amount ${evt.newUdAmount}`);
          newData.udReeval.push({
            blockNumber: block.header.height,
            timestamp: new Date(block.header.timestamp!),
            newUdAmount: evt.newUdAmount,
            monetaryMass: evt.monetaryMass,
            membersCount: evt.membersCount,
            event: event,
          });
          break;
        }

        default:
          // ctx.log.info(`Unhandled event ${event.name}`)
          break;
      }
    });

    // If the accumulator updated this block
    // push it in the history.
    if (acc.blockNumber == block.header.height) {
      newData.populationHistories.push({ ...acc });
    }
  });
}
