import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const validators =  {
    /**
     *  The current set of validators.
     */
    v800: new StorageType('Session.Validators', 'Default', [], sts.array(() => v800.AccountId32)) as ValidatorsV800,
}

/**
 *  The current set of validators.
 */
export interface ValidatorsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.AccountId32[]
    get(block: Block): Promise<(v800.AccountId32[] | undefined)>
}

export const currentIndex =  {
    /**
     *  Current index of the session.
     */
    v800: new StorageType('Session.CurrentIndex', 'Default', [], sts.number()) as CurrentIndexV800,
}

/**
 *  Current index of the session.
 */
export interface CurrentIndexV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const queuedChanged =  {
    /**
     *  True if the underlying economic identities or weighting behind the validators
     *  has changed in the queued validator set.
     */
    v800: new StorageType('Session.QueuedChanged', 'Default', [], sts.boolean()) as QueuedChangedV800,
}

/**
 *  True if the underlying economic identities or weighting behind the validators
 *  has changed in the queued validator set.
 */
export interface QueuedChangedV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): boolean
    get(block: Block): Promise<(boolean | undefined)>
}

export const queuedKeys =  {
    /**
     *  The queued keys for the next session. When the next session begins, these keys
     *  will be used to determine the validator's session keys.
     */
    v800: new StorageType('Session.QueuedKeys', 'Default', [], sts.array(() => sts.tuple(() => [v800.AccountId32, v800.SessionKeys]))) as QueuedKeysV800,
}

/**
 *  The queued keys for the next session. When the next session begins, these keys
 *  will be used to determine the validator's session keys.
 */
export interface QueuedKeysV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): [v800.AccountId32, v800.SessionKeys][]
    get(block: Block): Promise<([v800.AccountId32, v800.SessionKeys][] | undefined)>
}

export const disabledValidators =  {
    /**
     *  Indices of disabled validators.
     * 
     *  The vec is always kept sorted so that we can find whether a given validator is
     *  disabled using binary search. It gets cleared when `on_session_ending` returns
     *  a new set of identities.
     */
    v800: new StorageType('Session.DisabledValidators', 'Default', [], sts.array(() => sts.number())) as DisabledValidatorsV800,
}

/**
 *  Indices of disabled validators.
 * 
 *  The vec is always kept sorted so that we can find whether a given validator is
 *  disabled using binary search. It gets cleared when `on_session_ending` returns
 *  a new set of identities.
 */
export interface DisabledValidatorsV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block): Promise<(number[] | undefined)>
}

export const nextKeys =  {
    /**
     *  The next session keys for a validator.
     */
    v800: new StorageType('Session.NextKeys', 'Optional', [v800.AccountId32], v800.SessionKeys) as NextKeysV800,
}

/**
 *  The next session keys for a validator.
 */
export interface NextKeysV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: v800.AccountId32): Promise<(v800.SessionKeys | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(v800.SessionKeys | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (v800.SessionKeys | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (v800.SessionKeys | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (v800.SessionKeys | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (v800.SessionKeys | undefined)][]>
}

export const keyOwner =  {
    /**
     *  The owner of a key. The key is the `KeyTypeId` + the encoded key.
     */
    v800: new StorageType('Session.KeyOwner', 'Optional', [sts.tuple(() => [v800.KeyTypeId, sts.bytes()])], v800.AccountId32) as KeyOwnerV800,
}

/**
 *  The owner of a key. The key is the `KeyTypeId` + the encoded key.
 */
export interface KeyOwnerV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: [v800.KeyTypeId, Bytes]): Promise<(v800.AccountId32 | undefined)>
    getMany(block: Block, keys: [v800.KeyTypeId, Bytes][]): Promise<(v800.AccountId32 | undefined)[]>
    getKeys(block: Block): Promise<[v800.KeyTypeId, Bytes][]>
    getKeys(block: Block, key: [v800.KeyTypeId, Bytes]): Promise<[v800.KeyTypeId, Bytes][]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<[v800.KeyTypeId, Bytes][]>
    getKeysPaged(pageSize: number, block: Block, key: [v800.KeyTypeId, Bytes]): AsyncIterable<[v800.KeyTypeId, Bytes][]>
    getPairs(block: Block): Promise<[k: [v800.KeyTypeId, Bytes], v: (v800.AccountId32 | undefined)][]>
    getPairs(block: Block, key: [v800.KeyTypeId, Bytes]): Promise<[k: [v800.KeyTypeId, Bytes], v: (v800.AccountId32 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: [v800.KeyTypeId, Bytes], v: (v800.AccountId32 | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: [v800.KeyTypeId, Bytes]): AsyncIterable<[k: [v800.KeyTypeId, Bytes], v: (v800.AccountId32 | undefined)][]>
}
