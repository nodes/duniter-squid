import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const membership =  {
    /**
     *  maps identity id to membership data
     */
    v800: new StorageType('Membership.Membership', 'Optional', [sts.number()], v800.MembershipData) as MembershipV800,
}

/**
 *  maps identity id to membership data
 */
export interface MembershipV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(v800.MembershipData | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.MembershipData | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.MembershipData | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.MembershipData | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.MembershipData | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.MembershipData | undefined)][]>
}

export const counterForMembership =  {
    /**
     * Counter for the related counted storage map
     */
    v800: new StorageType('Membership.CounterForMembership', 'Default', [], sts.number()) as CounterForMembershipV800,
}

/**
 * Counter for the related counted storage map
 */
export interface CounterForMembershipV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const membershipsExpireOn =  {
    /**
     *  maps block number to the list of identity id set to expire at this block
     */
    v800: new StorageType('Membership.MembershipsExpireOn', 'Default', [sts.number()], sts.array(() => sts.number())) as MembershipsExpireOnV800,
}

/**
 *  maps block number to the list of identity id set to expire at this block
 */
export interface MembershipsExpireOnV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block, key: number): Promise<(number[] | undefined)>
    getMany(block: Block, keys: number[]): Promise<(number[] | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (number[] | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (number[] | undefined)][]>
}
