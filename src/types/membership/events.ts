import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const membershipAdded =  {
    name: 'Membership.MembershipAdded',
    /**
     * A membership was added.
     */
    v800: new EventType(
        'Membership.MembershipAdded',
        sts.struct({
            member: sts.number(),
            expireOn: sts.number(),
        })
    ),
}

export const membershipRenewed =  {
    name: 'Membership.MembershipRenewed',
    /**
     * A membership was renewed.
     */
    v800: new EventType(
        'Membership.MembershipRenewed',
        sts.struct({
            member: sts.number(),
            expireOn: sts.number(),
        })
    ),
}

export const membershipRemoved =  {
    name: 'Membership.MembershipRemoved',
    /**
     * A membership was removed.
     */
    v800: new EventType(
        'Membership.MembershipRemoved',
        sts.struct({
            member: sts.number(),
            reason: v800.MembershipRemovalReason,
        })
    ),
}
