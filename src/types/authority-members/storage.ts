import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const incomingAuthorities =  {
    /**
     *  list incoming authorities
     */
    v800: new StorageType('AuthorityMembers.IncomingAuthorities', 'Default', [], sts.array(() => sts.number())) as IncomingAuthoritiesV800,
}

/**
 *  list incoming authorities
 */
export interface IncomingAuthoritiesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block): Promise<(number[] | undefined)>
}

export const onlineAuthorities =  {
    /**
     *  list online authorities
     */
    v800: new StorageType('AuthorityMembers.OnlineAuthorities', 'Default', [], sts.array(() => sts.number())) as OnlineAuthoritiesV800,
}

/**
 *  list online authorities
 */
export interface OnlineAuthoritiesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block): Promise<(number[] | undefined)>
}

export const outgoingAuthorities =  {
    /**
     *  list outgoing authorities
     */
    v800: new StorageType('AuthorityMembers.OutgoingAuthorities', 'Default', [], sts.array(() => sts.number())) as OutgoingAuthoritiesV800,
}

/**
 *  list outgoing authorities
 */
export interface OutgoingAuthoritiesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block): Promise<(number[] | undefined)>
}

export const members =  {
    /**
     *  maps member id to member data
     */
    v800: new StorageType('AuthorityMembers.Members', 'Optional', [sts.number()], v800.MemberData) as MembersV800,
}

/**
 *  maps member id to member data
 */
export interface MembersV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(v800.MemberData | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.MemberData | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.MemberData | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.MemberData | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.MemberData | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.MemberData | undefined)][]>
}

export const blacklist =  {
    v800: new StorageType('AuthorityMembers.Blacklist', 'Default', [], sts.array(() => sts.number())) as BlacklistV800,
}

export interface BlacklistV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block): Promise<(number[] | undefined)>
}
