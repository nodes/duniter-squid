import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'

export const incomingAuthorities =  {
    name: 'AuthorityMembers.IncomingAuthorities',
    /**
     * List of members scheduled to join the set of authorities in the next session.
     */
    v800: new EventType(
        'AuthorityMembers.IncomingAuthorities',
        sts.struct({
            members: sts.array(() => sts.number()),
        })
    ),
}

export const outgoingAuthorities =  {
    name: 'AuthorityMembers.OutgoingAuthorities',
    /**
     * List of members leaving the set of authorities in the next session.
     */
    v800: new EventType(
        'AuthorityMembers.OutgoingAuthorities',
        sts.struct({
            members: sts.array(() => sts.number()),
        })
    ),
}

export const memberGoOffline =  {
    name: 'AuthorityMembers.MemberGoOffline',
    /**
     * A member will leave the set of authorities in 2 sessions.
     */
    v800: new EventType(
        'AuthorityMembers.MemberGoOffline',
        sts.struct({
            member: sts.number(),
        })
    ),
}

export const memberGoOnline =  {
    name: 'AuthorityMembers.MemberGoOnline',
    /**
     * A member will join the set of authorities in 2 sessions.
     */
    v800: new EventType(
        'AuthorityMembers.MemberGoOnline',
        sts.struct({
            member: sts.number(),
        })
    ),
}

export const memberRemoved =  {
    name: 'AuthorityMembers.MemberRemoved',
    /**
     * A member, who no longer has authority rights, will be removed from the authority set in 2 sessions.
     */
    v800: new EventType(
        'AuthorityMembers.MemberRemoved',
        sts.struct({
            member: sts.number(),
        })
    ),
}

export const memberRemovedFromBlacklist =  {
    name: 'AuthorityMembers.MemberRemovedFromBlacklist',
    /**
     * A member has been removed from the blacklist.
     */
    v800: new EventType(
        'AuthorityMembers.MemberRemovedFromBlacklist',
        sts.struct({
            member: sts.number(),
        })
    ),
}

export const memberAddedToBlacklist =  {
    name: 'AuthorityMembers.MemberAddedToBlacklist',
    /**
     * A member has been blacklisted.
     */
    v800: new EventType(
        'AuthorityMembers.MemberAddedToBlacklist',
        sts.struct({
            member: sts.number(),
        })
    ),
}
