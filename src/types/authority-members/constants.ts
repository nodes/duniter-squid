import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'

export const maxAuthorities =  {
    /**
     *  Max number of authorities allowed
     */
    v800: new ConstantType(
        'AuthorityMembers.MaxAuthorities',
        sts.number()
    ),
}
