import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const smiths =  {
    /**
     *  maps identity index to smith status
     */
    v800: new StorageType('SmithMembers.Smiths', 'Optional', [sts.number()], v800.SmithMeta) as SmithsV800,
}

/**
 *  maps identity index to smith status
 */
export interface SmithsV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(v800.SmithMeta | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.SmithMeta | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.SmithMeta | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.SmithMeta | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.SmithMeta | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.SmithMeta | undefined)][]>
}

export const expiresOn =  {
    /**
     *  maps session index to possible smith removals
     */
    v800: new StorageType('SmithMembers.ExpiresOn', 'Optional', [sts.number()], sts.array(() => sts.number())) as ExpiresOnV800,
}

/**
 *  maps session index to possible smith removals
 */
export interface ExpiresOnV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(number[] | undefined)>
    getMany(block: Block, keys: number[]): Promise<(number[] | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (number[] | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (number[] | undefined)][]>
}

export const currentSession =  {
    /**
     *  stores the current session index
     */
    v800: new StorageType('SmithMembers.CurrentSession', 'Default', [], sts.number()) as CurrentSessionV800,
}

/**
 *  stores the current session index
 */
export interface CurrentSessionV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}
