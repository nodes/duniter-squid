import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'

export const invitationSent =  {
    name: 'SmithMembers.InvitationSent',
    /**
     * An identity is being inivited to become a smith.
     */
    v800: new EventType(
        'SmithMembers.InvitationSent',
        sts.struct({
            receiver: sts.number(),
            issuer: sts.number(),
        })
    ),
}

export const invitationAccepted =  {
    name: 'SmithMembers.InvitationAccepted',
    /**
     * The invitation has been accepted.
     */
    v800: new EventType(
        'SmithMembers.InvitationAccepted',
        sts.struct({
            idtyIndex: sts.number(),
        })
    ),
}

export const smithCertAdded =  {
    name: 'SmithMembers.SmithCertAdded',
    /**
     * Certification received
     */
    v800: new EventType(
        'SmithMembers.SmithCertAdded',
        sts.struct({
            receiver: sts.number(),
            issuer: sts.number(),
        })
    ),
}

export const smithCertRemoved =  {
    name: 'SmithMembers.SmithCertRemoved',
    /**
     * Certification lost
     */
    v800: new EventType(
        'SmithMembers.SmithCertRemoved',
        sts.struct({
            receiver: sts.number(),
            issuer: sts.number(),
        })
    ),
}

export const smithMembershipAdded =  {
    name: 'SmithMembers.SmithMembershipAdded',
    /**
     * A smith gathered enough certifications to become an authority (can call `go_online()`).
     */
    v800: new EventType(
        'SmithMembers.SmithMembershipAdded',
        sts.struct({
            idtyIndex: sts.number(),
        })
    ),
}

export const smithMembershipRemoved =  {
    name: 'SmithMembers.SmithMembershipRemoved',
    /**
     * A smith has been removed from the smiths set.
     */
    v800: new EventType(
        'SmithMembers.SmithMembershipRemoved',
        sts.struct({
            idtyIndex: sts.number(),
        })
    ),
}
