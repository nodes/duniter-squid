import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const idtyCreated =  {
    name: 'Identity.IdtyCreated',
    /**
     * A new identity has been created.
     */
    v800: new EventType(
        'Identity.IdtyCreated',
        sts.struct({
            idtyIndex: sts.number(),
            ownerKey: v800.AccountId32,
        })
    ),
}

export const idtyConfirmed =  {
    name: 'Identity.IdtyConfirmed',
    /**
     * An identity has been confirmed by its owner.
     */
    v800: new EventType(
        'Identity.IdtyConfirmed',
        sts.struct({
            idtyIndex: sts.number(),
            ownerKey: v800.AccountId32,
            name: v800.IdtyName,
        })
    ),
}

export const idtyValidated =  {
    name: 'Identity.IdtyValidated',
    /**
     * An identity has been validated.
     */
    v800: new EventType(
        'Identity.IdtyValidated',
        sts.struct({
            idtyIndex: sts.number(),
        })
    ),
}

export const idtyChangedOwnerKey =  {
    name: 'Identity.IdtyChangedOwnerKey',
    v800: new EventType(
        'Identity.IdtyChangedOwnerKey',
        sts.struct({
            idtyIndex: sts.number(),
            newOwnerKey: v800.AccountId32,
        })
    ),
}

export const idtyRevoked =  {
    name: 'Identity.IdtyRevoked',
    /**
     * An identity has been revoked.
     */
    v800: new EventType(
        'Identity.IdtyRevoked',
        sts.struct({
            idtyIndex: sts.number(),
            reason: v800.RevocationReason,
        })
    ),
}

export const idtyRemoved =  {
    name: 'Identity.IdtyRemoved',
    /**
     * An identity has been removed.
     */
    v800: new EventType(
        'Identity.IdtyRemoved',
        sts.struct({
            idtyIndex: sts.number(),
            reason: v800.RemovalReason,
        })
    ),
}
