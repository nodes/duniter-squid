import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const identities =  {
    /**
     *  maps identity index to identity value
     */
    v800: new StorageType('Identity.Identities', 'Optional', [sts.number()], v800.IdtyValue) as IdentitiesV800,
}

/**
 *  maps identity index to identity value
 */
export interface IdentitiesV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(v800.IdtyValue | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.IdtyValue | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.IdtyValue | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.IdtyValue | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.IdtyValue | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.IdtyValue | undefined)][]>
}

export const counterForIdentities =  {
    /**
     * Counter for the related counted storage map
     */
    v800: new StorageType('Identity.CounterForIdentities', 'Default', [], sts.number()) as CounterForIdentitiesV800,
}

/**
 * Counter for the related counted storage map
 */
export interface CounterForIdentitiesV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const identityIndexOf =  {
    /**
     *  maps account id to identity index
     */
    v800: new StorageType('Identity.IdentityIndexOf', 'Optional', [v800.AccountId32], sts.number()) as IdentityIndexOfV800,
}

/**
 *  maps account id to identity index
 */
export interface IdentityIndexOfV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: v800.AccountId32): Promise<(number | undefined)>
    getMany(block: Block, keys: v800.AccountId32[]): Promise<(number | undefined)[]>
    getKeys(block: Block): Promise<v800.AccountId32[]>
    getKeys(block: Block, key: v800.AccountId32): Promise<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.AccountId32[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<v800.AccountId32[]>
    getPairs(block: Block): Promise<[k: v800.AccountId32, v: (number | undefined)][]>
    getPairs(block: Block, key: v800.AccountId32): Promise<[k: v800.AccountId32, v: (number | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.AccountId32, v: (number | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.AccountId32): AsyncIterable<[k: v800.AccountId32, v: (number | undefined)][]>
}

export const identitiesNames =  {
    /**
     *  maps identity name to identity index (simply a set)
     */
    v800: new StorageType('Identity.IdentitiesNames', 'Optional', [v800.IdtyName], sts.number()) as IdentitiesNamesV800,
}

/**
 *  maps identity name to identity index (simply a set)
 */
export interface IdentitiesNamesV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: v800.IdtyName): Promise<(number | undefined)>
    getMany(block: Block, keys: v800.IdtyName[]): Promise<(number | undefined)[]>
    getKeys(block: Block): Promise<v800.IdtyName[]>
    getKeys(block: Block, key: v800.IdtyName): Promise<v800.IdtyName[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<v800.IdtyName[]>
    getKeysPaged(pageSize: number, block: Block, key: v800.IdtyName): AsyncIterable<v800.IdtyName[]>
    getPairs(block: Block): Promise<[k: v800.IdtyName, v: (number | undefined)][]>
    getPairs(block: Block, key: v800.IdtyName): Promise<[k: v800.IdtyName, v: (number | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: v800.IdtyName, v: (number | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: v800.IdtyName): AsyncIterable<[k: v800.IdtyName, v: (number | undefined)][]>
}

export const nextIdtyIndex =  {
    /**
     *  counter of the identity index to give to the next identity
     */
    v800: new StorageType('Identity.NextIdtyIndex', 'Default', [], sts.number()) as NextIdtyIndexV800,
}

/**
 *  counter of the identity index to give to the next identity
 */
export interface NextIdtyIndexV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number
    get(block: Block): Promise<(number | undefined)>
}

export const identityChangeSchedule =  {
    /**
     *  maps block number to the list of identities set to be removed at this bloc
     */
    v800: new StorageType('Identity.IdentityChangeSchedule', 'Default', [sts.number()], sts.array(() => sts.number())) as IdentityChangeScheduleV800,
}

/**
 *  maps block number to the list of identities set to be removed at this bloc
 */
export interface IdentityChangeScheduleV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): number[]
    get(block: Block, key: number): Promise<(number[] | undefined)>
    getMany(block: Block, keys: number[]): Promise<(number[] | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (number[] | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (number[] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (number[] | undefined)][]>
}
