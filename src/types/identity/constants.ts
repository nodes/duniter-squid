import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'

export const confirmPeriod =  {
    /**
     *  Period during which the owner can confirm the new identity.
     */
    v800: new ConstantType(
        'Identity.ConfirmPeriod',
        sts.number()
    ),
}

export const validationPeriod =  {
    /**
     *  Period before which the identity has to be validated (become member).
     */
    v800: new ConstantType(
        'Identity.ValidationPeriod',
        sts.number()
    ),
}

export const autorevocationPeriod =  {
    /**
     *  Period before which an identity who lost membership is automatically revoked.
     */
    v800: new ConstantType(
        'Identity.AutorevocationPeriod',
        sts.number()
    ),
}

export const deletionPeriod =  {
    /**
     *  Period after which a revoked identity is removed and the keys are freed.
     */
    v800: new ConstantType(
        'Identity.DeletionPeriod',
        sts.number()
    ),
}

export const changeOwnerKeyPeriod =  {
    /**
     *  Minimum duration between two owner key changes.
     */
    v800: new ConstantType(
        'Identity.ChangeOwnerKeyPeriod',
        sts.number()
    ),
}

export const idtyCreationPeriod =  {
    /**
     *  Minimum duration between the creation of 2 identities by the same creator.
     */
    v800: new ConstantType(
        'Identity.IdtyCreationPeriod',
        sts.number()
    ),
}
