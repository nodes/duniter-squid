import {sts, Result, Option, Bytes, BitSequence} from './support'

export type AccountId32 = Bytes

export interface IdAmount {
    id: RuntimeHoldReason
    amount: bigint
}

export type RuntimeHoldReason = RuntimeHoldReason_Distance | RuntimeHoldReason_Preimage

export interface RuntimeHoldReason_Distance {
    __kind: 'Distance'
    value: Type_216
}

export interface RuntimeHoldReason_Preimage {
    __kind: 'Preimage'
    value: HoldReason
}

export type HoldReason = HoldReason_Preimage

export interface HoldReason_Preimage {
    __kind: 'Preimage'
}

export type Type_216 = Type_216_DistanceHold

export interface Type_216_DistanceHold {
    __kind: 'DistanceHold'
}

export const IdAmount: sts.Type<IdAmount> = sts.struct(() => {
    return  {
        id: RuntimeHoldReason,
        amount: sts.bigint(),
    }
})

export const RuntimeHoldReason: sts.Type<RuntimeHoldReason> = sts.closedEnum(() => {
    return  {
        Distance: Type_216,
        Preimage: HoldReason,
    }
})

export const HoldReason: sts.Type<HoldReason> = sts.closedEnum(() => {
    return  {
        Preimage: sts.unit(),
    }
})

export const Type_216: sts.Type<Type_216> = sts.closedEnum(() => {
    return  {
        DistanceHold: sts.unit(),
    }
})

export const AccountId32 = sts.bytes()

export interface EventRecord {
    phase: Phase
    event: Event
    topics: H256[]
}

export type H256 = Bytes

export type Event = Event_Account | Event_AtomicSwap | Event_AuthorityMembers | Event_Balances | Event_Certification | Event_Distance | Event_Grandpa | Event_Identity | Event_ImOnline | Event_Membership | Event_Multisig | Event_Offences | Event_OneshotAccount | Event_Preimage | Event_ProvideRandomness | Event_Proxy | Event_Quota | Event_Scheduler | Event_Session | Event_SmithMembers | Event_Sudo | Event_System | Event_TechnicalCommittee | Event_TransactionPayment | Event_Treasury | Event_UniversalDividend | Event_UpgradeOrigin | Event_Utility

export interface Event_Account {
    __kind: 'Account'
    value: AccountEvent
}

export interface Event_AtomicSwap {
    __kind: 'AtomicSwap'
    value: AtomicSwapEvent
}

export interface Event_AuthorityMembers {
    __kind: 'AuthorityMembers'
    value: AuthorityMembersEvent
}

export interface Event_Balances {
    __kind: 'Balances'
    value: BalancesEvent
}

export interface Event_Certification {
    __kind: 'Certification'
    value: CertificationEvent
}

export interface Event_Distance {
    __kind: 'Distance'
    value: DistanceEvent
}

export interface Event_Grandpa {
    __kind: 'Grandpa'
    value: GrandpaEvent
}

export interface Event_Identity {
    __kind: 'Identity'
    value: IdentityEvent
}

export interface Event_ImOnline {
    __kind: 'ImOnline'
    value: ImOnlineEvent
}

export interface Event_Membership {
    __kind: 'Membership'
    value: MembershipEvent
}

export interface Event_Multisig {
    __kind: 'Multisig'
    value: MultisigEvent
}

export interface Event_Offences {
    __kind: 'Offences'
    value: OffencesEvent
}

export interface Event_OneshotAccount {
    __kind: 'OneshotAccount'
    value: OneshotAccountEvent
}

export interface Event_Preimage {
    __kind: 'Preimage'
    value: PreimageEvent
}

export interface Event_ProvideRandomness {
    __kind: 'ProvideRandomness'
    value: ProvideRandomnessEvent
}

export interface Event_Proxy {
    __kind: 'Proxy'
    value: ProxyEvent
}

export interface Event_Quota {
    __kind: 'Quota'
    value: QuotaEvent
}

export interface Event_Scheduler {
    __kind: 'Scheduler'
    value: SchedulerEvent
}

export interface Event_Session {
    __kind: 'Session'
    value: SessionEvent
}

export interface Event_SmithMembers {
    __kind: 'SmithMembers'
    value: SmithMembersEvent
}

export interface Event_Sudo {
    __kind: 'Sudo'
    value: SudoEvent
}

export interface Event_System {
    __kind: 'System'
    value: SystemEvent
}

export interface Event_TechnicalCommittee {
    __kind: 'TechnicalCommittee'
    value: TechnicalCommitteeEvent
}

export interface Event_TransactionPayment {
    __kind: 'TransactionPayment'
    value: TransactionPaymentEvent
}

export interface Event_Treasury {
    __kind: 'Treasury'
    value: TreasuryEvent
}

export interface Event_UniversalDividend {
    __kind: 'UniversalDividend'
    value: UniversalDividendEvent
}

export interface Event_UpgradeOrigin {
    __kind: 'UpgradeOrigin'
    value: UpgradeOriginEvent
}

export interface Event_Utility {
    __kind: 'Utility'
    value: UtilityEvent
}

/**
 * The `Event` enum of this pallet
 */
export type UtilityEvent = UtilityEvent_BatchCompleted | UtilityEvent_BatchCompletedWithErrors | UtilityEvent_BatchInterrupted | UtilityEvent_DispatchedAs | UtilityEvent_ItemCompleted | UtilityEvent_ItemFailed

/**
 * Batch of dispatches completed fully with no error.
 */
export interface UtilityEvent_BatchCompleted {
    __kind: 'BatchCompleted'
}

/**
 * Batch of dispatches completed but has errors.
 */
export interface UtilityEvent_BatchCompletedWithErrors {
    __kind: 'BatchCompletedWithErrors'
}

/**
 * Batch of dispatches did not complete fully. Index of first failing dispatch given, as
 * well as the error.
 */
export interface UtilityEvent_BatchInterrupted {
    __kind: 'BatchInterrupted'
    index: number
    error: DispatchError
}

/**
 * A call was dispatched.
 */
export interface UtilityEvent_DispatchedAs {
    __kind: 'DispatchedAs'
    result: Result<null, DispatchError>
}

/**
 * A single item within a Batch of dispatches has completed with no error.
 */
export interface UtilityEvent_ItemCompleted {
    __kind: 'ItemCompleted'
}

/**
 * A single item within a Batch of dispatches has completed with error.
 */
export interface UtilityEvent_ItemFailed {
    __kind: 'ItemFailed'
    error: DispatchError
}

export type DispatchError = DispatchError_Arithmetic | DispatchError_BadOrigin | DispatchError_CannotLookup | DispatchError_ConsumerRemaining | DispatchError_Corruption | DispatchError_Exhausted | DispatchError_Module | DispatchError_NoProviders | DispatchError_Other | DispatchError_RootNotAllowed | DispatchError_Token | DispatchError_TooManyConsumers | DispatchError_Transactional | DispatchError_Unavailable

export interface DispatchError_Arithmetic {
    __kind: 'Arithmetic'
    value: ArithmeticError
}

export interface DispatchError_BadOrigin {
    __kind: 'BadOrigin'
}

export interface DispatchError_CannotLookup {
    __kind: 'CannotLookup'
}

export interface DispatchError_ConsumerRemaining {
    __kind: 'ConsumerRemaining'
}

export interface DispatchError_Corruption {
    __kind: 'Corruption'
}

export interface DispatchError_Exhausted {
    __kind: 'Exhausted'
}

export interface DispatchError_Module {
    __kind: 'Module'
    value: ModuleError
}

export interface DispatchError_NoProviders {
    __kind: 'NoProviders'
}

export interface DispatchError_Other {
    __kind: 'Other'
}

export interface DispatchError_RootNotAllowed {
    __kind: 'RootNotAllowed'
}

export interface DispatchError_Token {
    __kind: 'Token'
    value: TokenError
}

export interface DispatchError_TooManyConsumers {
    __kind: 'TooManyConsumers'
}

export interface DispatchError_Transactional {
    __kind: 'Transactional'
    value: TransactionalError
}

export interface DispatchError_Unavailable {
    __kind: 'Unavailable'
}

export type TransactionalError = TransactionalError_LimitReached | TransactionalError_NoLayer

export interface TransactionalError_LimitReached {
    __kind: 'LimitReached'
}

export interface TransactionalError_NoLayer {
    __kind: 'NoLayer'
}

export type TokenError = TokenError_BelowMinimum | TokenError_Blocked | TokenError_CannotCreate | TokenError_CannotCreateHold | TokenError_Frozen | TokenError_FundsUnavailable | TokenError_NotExpendable | TokenError_OnlyProvider | TokenError_UnknownAsset | TokenError_Unsupported

export interface TokenError_BelowMinimum {
    __kind: 'BelowMinimum'
}

export interface TokenError_Blocked {
    __kind: 'Blocked'
}

export interface TokenError_CannotCreate {
    __kind: 'CannotCreate'
}

export interface TokenError_CannotCreateHold {
    __kind: 'CannotCreateHold'
}

export interface TokenError_Frozen {
    __kind: 'Frozen'
}

export interface TokenError_FundsUnavailable {
    __kind: 'FundsUnavailable'
}

export interface TokenError_NotExpendable {
    __kind: 'NotExpendable'
}

export interface TokenError_OnlyProvider {
    __kind: 'OnlyProvider'
}

export interface TokenError_UnknownAsset {
    __kind: 'UnknownAsset'
}

export interface TokenError_Unsupported {
    __kind: 'Unsupported'
}

export interface ModuleError {
    index: number
    error: Bytes
}

export type ArithmeticError = ArithmeticError_DivisionByZero | ArithmeticError_Overflow | ArithmeticError_Underflow

export interface ArithmeticError_DivisionByZero {
    __kind: 'DivisionByZero'
}

export interface ArithmeticError_Overflow {
    __kind: 'Overflow'
}

export interface ArithmeticError_Underflow {
    __kind: 'Underflow'
}

/**
 * The `Event` enum of this pallet
 */
export type UpgradeOriginEvent = UpgradeOriginEvent_DispatchedAsRoot

/**
 * A call was dispatched as root from an upgradable origin
 */
export interface UpgradeOriginEvent_DispatchedAsRoot {
    __kind: 'DispatchedAsRoot'
    result: Result<null, DispatchError>
}

/**
 * The `Event` enum of this pallet
 */
export type UniversalDividendEvent = UniversalDividendEvent_NewUdCreated | UniversalDividendEvent_UdReevalued | UniversalDividendEvent_UdsAutoPaid | UniversalDividendEvent_UdsClaimed

/**
 * A new universal dividend is created.
 */
export interface UniversalDividendEvent_NewUdCreated {
    __kind: 'NewUdCreated'
    amount: bigint
    index: number
    monetaryMass: bigint
    membersCount: bigint
}

/**
 * The universal dividend has been re-evaluated.
 */
export interface UniversalDividendEvent_UdReevalued {
    __kind: 'UdReevalued'
    newUdAmount: bigint
    monetaryMass: bigint
    membersCount: bigint
}

/**
 * DUs were automatically transferred as part of a member removal.
 */
export interface UniversalDividendEvent_UdsAutoPaid {
    __kind: 'UdsAutoPaid'
    count: number
    total: bigint
    who: AccountId32
}

/**
 * A member claimed his UDs.
 */
export interface UniversalDividendEvent_UdsClaimed {
    __kind: 'UdsClaimed'
    count: number
    total: bigint
    who: AccountId32
}

/**
 * The `Event` enum of this pallet
 */
export type TreasuryEvent = TreasuryEvent_AssetSpendApproved | TreasuryEvent_AssetSpendVoided | TreasuryEvent_Awarded | TreasuryEvent_Burnt | TreasuryEvent_Deposit | TreasuryEvent_Paid | TreasuryEvent_PaymentFailed | TreasuryEvent_Rollover | TreasuryEvent_SpendApproved | TreasuryEvent_SpendProcessed | TreasuryEvent_Spending | TreasuryEvent_UpdatedInactive

/**
 * A new asset spend proposal has been approved.
 */
export interface TreasuryEvent_AssetSpendApproved {
    __kind: 'AssetSpendApproved'
    index: number
    amount: bigint
    beneficiary: AccountId32
    validFrom: number
    expireAt: number
}

/**
 * An approved spend was voided.
 */
export interface TreasuryEvent_AssetSpendVoided {
    __kind: 'AssetSpendVoided'
    index: number
}

/**
 * Some funds have been allocated.
 */
export interface TreasuryEvent_Awarded {
    __kind: 'Awarded'
    proposalIndex: number
    award: bigint
    account: AccountId32
}

/**
 * Some of our funds have been burnt.
 */
export interface TreasuryEvent_Burnt {
    __kind: 'Burnt'
    burntFunds: bigint
}

/**
 * Some funds have been deposited.
 */
export interface TreasuryEvent_Deposit {
    __kind: 'Deposit'
    value: bigint
}

/**
 * A payment happened.
 */
export interface TreasuryEvent_Paid {
    __kind: 'Paid'
    index: number
}

/**
 * A payment failed and can be retried.
 */
export interface TreasuryEvent_PaymentFailed {
    __kind: 'PaymentFailed'
    index: number
}

/**
 * Spending has finished; this is the amount that rolls over until next spend.
 */
export interface TreasuryEvent_Rollover {
    __kind: 'Rollover'
    rolloverBalance: bigint
}

/**
 * A new spend proposal has been approved.
 */
export interface TreasuryEvent_SpendApproved {
    __kind: 'SpendApproved'
    proposalIndex: number
    amount: bigint
    beneficiary: AccountId32
}

/**
 * A spend was processed and removed from the storage. It might have been successfully
 * paid or it may have expired.
 */
export interface TreasuryEvent_SpendProcessed {
    __kind: 'SpendProcessed'
    index: number
}

/**
 * We have ended a spend period and will now allocate funds.
 */
export interface TreasuryEvent_Spending {
    __kind: 'Spending'
    budgetRemaining: bigint
}

/**
 * The inactive funds of the pallet have been updated.
 */
export interface TreasuryEvent_UpdatedInactive {
    __kind: 'UpdatedInactive'
    reactivated: bigint
    deactivated: bigint
}

/**
 * The `Event` enum of this pallet
 */
export type TransactionPaymentEvent = TransactionPaymentEvent_TransactionFeePaid

/**
 * A transaction fee `actual_fee`, of which `tip` was added to the minimum inclusion fee,
 * has been paid by `who`.
 */
export interface TransactionPaymentEvent_TransactionFeePaid {
    __kind: 'TransactionFeePaid'
    who: AccountId32
    actualFee: bigint
    tip: bigint
}

/**
 * The `Event` enum of this pallet
 */
export type TechnicalCommitteeEvent = TechnicalCommitteeEvent_Approved | TechnicalCommitteeEvent_Closed | TechnicalCommitteeEvent_Disapproved | TechnicalCommitteeEvent_Executed | TechnicalCommitteeEvent_MemberExecuted | TechnicalCommitteeEvent_Proposed | TechnicalCommitteeEvent_Voted

/**
 * A motion was approved by the required threshold.
 */
export interface TechnicalCommitteeEvent_Approved {
    __kind: 'Approved'
    proposalHash: H256
}

/**
 * A proposal was closed because its threshold was reached or after its duration was up.
 */
export interface TechnicalCommitteeEvent_Closed {
    __kind: 'Closed'
    proposalHash: H256
    yes: number
    no: number
}

/**
 * A motion was not approved by the required threshold.
 */
export interface TechnicalCommitteeEvent_Disapproved {
    __kind: 'Disapproved'
    proposalHash: H256
}

/**
 * A motion was executed; result will be `Ok` if it returned without error.
 */
export interface TechnicalCommitteeEvent_Executed {
    __kind: 'Executed'
    proposalHash: H256
    result: Result<null, DispatchError>
}

/**
 * A single member did some action; result will be `Ok` if it returned without error.
 */
export interface TechnicalCommitteeEvent_MemberExecuted {
    __kind: 'MemberExecuted'
    proposalHash: H256
    result: Result<null, DispatchError>
}

/**
 * A motion (given hash) has been proposed (by given account) with a threshold (given
 * `MemberCount`).
 */
export interface TechnicalCommitteeEvent_Proposed {
    __kind: 'Proposed'
    account: AccountId32
    proposalIndex: number
    proposalHash: H256
    threshold: number
}

/**
 * A motion (given hash) has been voted on by given account, leaving
 * a tally (yes votes and no votes given respectively as `MemberCount`).
 */
export interface TechnicalCommitteeEvent_Voted {
    __kind: 'Voted'
    account: AccountId32
    proposalHash: H256
    voted: boolean
    yes: number
    no: number
}

/**
 * Event for the System pallet.
 */
export type SystemEvent = SystemEvent_CodeUpdated | SystemEvent_ExtrinsicFailed | SystemEvent_ExtrinsicSuccess | SystemEvent_KilledAccount | SystemEvent_NewAccount | SystemEvent_Remarked | SystemEvent_UpgradeAuthorized

/**
 * `:code` was updated.
 */
export interface SystemEvent_CodeUpdated {
    __kind: 'CodeUpdated'
}

/**
 * An extrinsic failed.
 */
export interface SystemEvent_ExtrinsicFailed {
    __kind: 'ExtrinsicFailed'
    dispatchError: DispatchError
    dispatchInfo: DispatchInfo
}

/**
 * An extrinsic completed successfully.
 */
export interface SystemEvent_ExtrinsicSuccess {
    __kind: 'ExtrinsicSuccess'
    dispatchInfo: DispatchInfo
}

/**
 * An account was reaped.
 */
export interface SystemEvent_KilledAccount {
    __kind: 'KilledAccount'
    account: AccountId32
}

/**
 * A new account was created.
 */
export interface SystemEvent_NewAccount {
    __kind: 'NewAccount'
    account: AccountId32
}

/**
 * On on-chain remark happened.
 */
export interface SystemEvent_Remarked {
    __kind: 'Remarked'
    sender: AccountId32
    hash: H256
}

/**
 * An upgrade was authorized.
 */
export interface SystemEvent_UpgradeAuthorized {
    __kind: 'UpgradeAuthorized'
    codeHash: H256
    checkVersion: boolean
}

export interface DispatchInfo {
    weight: Weight
    class: DispatchClass
    paysFee: Pays
}

export type Pays = Pays_No | Pays_Yes

export interface Pays_No {
    __kind: 'No'
}

export interface Pays_Yes {
    __kind: 'Yes'
}

export type DispatchClass = DispatchClass_Mandatory | DispatchClass_Normal | DispatchClass_Operational

export interface DispatchClass_Mandatory {
    __kind: 'Mandatory'
}

export interface DispatchClass_Normal {
    __kind: 'Normal'
}

export interface DispatchClass_Operational {
    __kind: 'Operational'
}

export interface Weight {
    refTime: bigint
    proofSize: bigint
}

/**
 * The `Event` enum of this pallet
 */
export type SudoEvent = SudoEvent_KeyChanged | SudoEvent_KeyRemoved | SudoEvent_Sudid | SudoEvent_SudoAsDone

/**
 * The sudo key has been updated.
 */
export interface SudoEvent_KeyChanged {
    __kind: 'KeyChanged'
    /**
     * The old sudo key (if one was previously set).
     */
    old?: (AccountId32 | undefined)
    /**
     * The new sudo key (if one was set).
     */
    new: AccountId32
}

/**
 * The key was permanently removed.
 */
export interface SudoEvent_KeyRemoved {
    __kind: 'KeyRemoved'
}

/**
 * A sudo call just took place.
 */
export interface SudoEvent_Sudid {
    __kind: 'Sudid'
    /**
     * The result of the call made by the sudo user.
     */
    sudoResult: Result<null, DispatchError>
}

/**
 * A [sudo_as](Pallet::sudo_as) call just took place.
 */
export interface SudoEvent_SudoAsDone {
    __kind: 'SudoAsDone'
    /**
     * The result of the call made by the sudo user.
     */
    sudoResult: Result<null, DispatchError>
}

/**
 * Events type.
 */
export type SmithMembersEvent = SmithMembersEvent_InvitationAccepted | SmithMembersEvent_InvitationSent | SmithMembersEvent_SmithCertAdded | SmithMembersEvent_SmithCertRemoved | SmithMembersEvent_SmithMembershipAdded | SmithMembersEvent_SmithMembershipRemoved

/**
 * The invitation has been accepted.
 */
export interface SmithMembersEvent_InvitationAccepted {
    __kind: 'InvitationAccepted'
    idtyIndex: number
}

/**
 * An identity is being inivited to become a smith.
 */
export interface SmithMembersEvent_InvitationSent {
    __kind: 'InvitationSent'
    issuer: number
    receiver: number
}

/**
 * Certification received
 */
export interface SmithMembersEvent_SmithCertAdded {
    __kind: 'SmithCertAdded'
    issuer: number
    receiver: number
}

/**
 * Certification lost
 */
export interface SmithMembersEvent_SmithCertRemoved {
    __kind: 'SmithCertRemoved'
    issuer: number
    receiver: number
}

/**
 * A smith gathered enough certifications to become an authority (can call `go_online()`).
 */
export interface SmithMembersEvent_SmithMembershipAdded {
    __kind: 'SmithMembershipAdded'
    idtyIndex: number
}

/**
 * A smith has been removed from the smiths set.
 */
export interface SmithMembersEvent_SmithMembershipRemoved {
    __kind: 'SmithMembershipRemoved'
    idtyIndex: number
}

/**
 * The `Event` enum of this pallet
 */
export type SessionEvent = SessionEvent_NewSession

/**
 * New session has happened. Note that the argument is the session index, not the
 * block number as the type might suggest.
 */
export interface SessionEvent_NewSession {
    __kind: 'NewSession'
    sessionIndex: number
}

/**
 * Events type.
 */
export type SchedulerEvent = SchedulerEvent_CallUnavailable | SchedulerEvent_Canceled | SchedulerEvent_Dispatched | SchedulerEvent_PeriodicFailed | SchedulerEvent_PermanentlyOverweight | SchedulerEvent_RetryCancelled | SchedulerEvent_RetryFailed | SchedulerEvent_RetrySet | SchedulerEvent_Scheduled

/**
 * The call for the provided hash was not found so the task has been aborted.
 */
export interface SchedulerEvent_CallUnavailable {
    __kind: 'CallUnavailable'
    task: [number, number]
    id?: (Bytes | undefined)
}

/**
 * Canceled some task.
 */
export interface SchedulerEvent_Canceled {
    __kind: 'Canceled'
    when: number
    index: number
}

/**
 * Dispatched some task.
 */
export interface SchedulerEvent_Dispatched {
    __kind: 'Dispatched'
    task: [number, number]
    id?: (Bytes | undefined)
    result: Result<null, DispatchError>
}

/**
 * The given task was unable to be renewed since the agenda is full at that block.
 */
export interface SchedulerEvent_PeriodicFailed {
    __kind: 'PeriodicFailed'
    task: [number, number]
    id?: (Bytes | undefined)
}

/**
 * The given task can never be executed since it is overweight.
 */
export interface SchedulerEvent_PermanentlyOverweight {
    __kind: 'PermanentlyOverweight'
    task: [number, number]
    id?: (Bytes | undefined)
}

/**
 * Cancel a retry configuration for some task.
 */
export interface SchedulerEvent_RetryCancelled {
    __kind: 'RetryCancelled'
    task: [number, number]
    id?: (Bytes | undefined)
}

/**
 * The given task was unable to be retried since the agenda is full at that block or there
 * was not enough weight to reschedule it.
 */
export interface SchedulerEvent_RetryFailed {
    __kind: 'RetryFailed'
    task: [number, number]
    id?: (Bytes | undefined)
}

/**
 * Set a retry configuration for some task.
 */
export interface SchedulerEvent_RetrySet {
    __kind: 'RetrySet'
    task: [number, number]
    id?: (Bytes | undefined)
    period: number
    retries: number
}

/**
 * Scheduled some task.
 */
export interface SchedulerEvent_Scheduled {
    __kind: 'Scheduled'
    when: number
    index: number
}

/**
 * The `Event` enum of this pallet
 */
export type QuotaEvent = QuotaEvent_NoMoreCurrencyForRefund | QuotaEvent_NoQuotaForIdty | QuotaEvent_RefundFailed | QuotaEvent_RefundQueueFull | QuotaEvent_Refunded

/**
 * No more currency available for refund.
 * This scenario should never occur if the fees are intended for the refund account.
 */
export interface QuotaEvent_NoMoreCurrencyForRefund {
    __kind: 'NoMoreCurrencyForRefund'
}

/**
 * No more quota available for refund.
 */
export interface QuotaEvent_NoQuotaForIdty {
    __kind: 'NoQuotaForIdty'
    value: number
}

/**
 * The refund has failed.
 * This scenario should rarely occur, except when the account was destroyed in the interim between the request and the refund.
 */
export interface QuotaEvent_RefundFailed {
    __kind: 'RefundFailed'
    value: AccountId32
}

/**
 * Refund queue was full.
 */
export interface QuotaEvent_RefundQueueFull {
    __kind: 'RefundQueueFull'
}

/**
 * Transaction fees were refunded.
 */
export interface QuotaEvent_Refunded {
    __kind: 'Refunded'
    who: AccountId32
    identity: number
    amount: bigint
}

/**
 * The `Event` enum of this pallet
 */
export type ProxyEvent = ProxyEvent_Announced | ProxyEvent_ProxyAdded | ProxyEvent_ProxyExecuted | ProxyEvent_ProxyRemoved | ProxyEvent_PureCreated

/**
 * An announcement was placed to make a call in the future.
 */
export interface ProxyEvent_Announced {
    __kind: 'Announced'
    real: AccountId32
    proxy: AccountId32
    callHash: H256
}

/**
 * A proxy was added.
 */
export interface ProxyEvent_ProxyAdded {
    __kind: 'ProxyAdded'
    delegator: AccountId32
    delegatee: AccountId32
    proxyType: ProxyType
    delay: number
}

/**
 * A proxy was executed correctly, with the given.
 */
export interface ProxyEvent_ProxyExecuted {
    __kind: 'ProxyExecuted'
    result: Result<null, DispatchError>
}

/**
 * A proxy was removed.
 */
export interface ProxyEvent_ProxyRemoved {
    __kind: 'ProxyRemoved'
    delegator: AccountId32
    delegatee: AccountId32
    proxyType: ProxyType
    delay: number
}

/**
 * A pure account has been created by new proxy with given
 * disambiguation index and proxy type.
 */
export interface ProxyEvent_PureCreated {
    __kind: 'PureCreated'
    pure: AccountId32
    who: AccountId32
    proxyType: ProxyType
    disambiguationIndex: number
}

export type ProxyType = ProxyType_AlmostAny | ProxyType_CancelProxy | ProxyType_TechnicalCommitteePropose | ProxyType_TransferOnly

export interface ProxyType_AlmostAny {
    __kind: 'AlmostAny'
}

export interface ProxyType_CancelProxy {
    __kind: 'CancelProxy'
}

export interface ProxyType_TechnicalCommitteePropose {
    __kind: 'TechnicalCommitteePropose'
}

export interface ProxyType_TransferOnly {
    __kind: 'TransferOnly'
}

/**
 * The `Event` enum of this pallet
 */
export type ProvideRandomnessEvent = ProvideRandomnessEvent_FilledRandomness | ProvideRandomnessEvent_RequestedRandomness

/**
 * A request for randomness was fulfilled.
 */
export interface ProvideRandomnessEvent_FilledRandomness {
    __kind: 'FilledRandomness'
    requestId: bigint
    randomness: H256
}

/**
 * A request for randomness was made.
 */
export interface ProvideRandomnessEvent_RequestedRandomness {
    __kind: 'RequestedRandomness'
    requestId: bigint
    salt: H256
    type: RandomnessType
}

export type RandomnessType = RandomnessType_RandomnessFromOneEpochAgo | RandomnessType_RandomnessFromPreviousBlock | RandomnessType_RandomnessFromTwoEpochsAgo

export interface RandomnessType_RandomnessFromOneEpochAgo {
    __kind: 'RandomnessFromOneEpochAgo'
}

export interface RandomnessType_RandomnessFromPreviousBlock {
    __kind: 'RandomnessFromPreviousBlock'
}

export interface RandomnessType_RandomnessFromTwoEpochsAgo {
    __kind: 'RandomnessFromTwoEpochsAgo'
}

/**
 * The `Event` enum of this pallet
 */
export type PreimageEvent = PreimageEvent_Cleared | PreimageEvent_Noted | PreimageEvent_Requested

/**
 * A preimage has ben cleared.
 */
export interface PreimageEvent_Cleared {
    __kind: 'Cleared'
    hash: H256
}

/**
 * A preimage has been noted.
 */
export interface PreimageEvent_Noted {
    __kind: 'Noted'
    hash: H256
}

/**
 * A preimage has been requested.
 */
export interface PreimageEvent_Requested {
    __kind: 'Requested'
    hash: H256
}

/**
 * The `Event` enum of this pallet
 */
export type OneshotAccountEvent = OneshotAccountEvent_OneshotAccountConsumed | OneshotAccountEvent_OneshotAccountCreated | OneshotAccountEvent_Withdraw

/**
 * A oneshot account was consumed.
 */
export interface OneshotAccountEvent_OneshotAccountConsumed {
    __kind: 'OneshotAccountConsumed'
    account: AccountId32
    dest1: [AccountId32, bigint]
    dest2?: ([AccountId32, bigint] | undefined)
}

/**
 * A oneshot account was created.
 */
export interface OneshotAccountEvent_OneshotAccountCreated {
    __kind: 'OneshotAccountCreated'
    account: AccountId32
    balance: bigint
    creator: AccountId32
}

/**
 * A withdrawal was executed on a oneshot account.
 */
export interface OneshotAccountEvent_Withdraw {
    __kind: 'Withdraw'
    account: AccountId32
    balance: bigint
}

/**
 * Events type.
 */
export type OffencesEvent = OffencesEvent_Offence

/**
 * An offense was reported during the specified time slot. This event is not deposited for duplicate slashes.
 */
export interface OffencesEvent_Offence {
    __kind: 'Offence'
    kind: Bytes
    timeslot: Bytes
}

/**
 * The `Event` enum of this pallet
 */
export type MultisigEvent = MultisigEvent_MultisigApproval | MultisigEvent_MultisigCancelled | MultisigEvent_MultisigExecuted | MultisigEvent_NewMultisig

/**
 * A multisig operation has been approved by someone.
 */
export interface MultisigEvent_MultisigApproval {
    __kind: 'MultisigApproval'
    approving: AccountId32
    timepoint: Timepoint
    multisig: AccountId32
    callHash: Bytes
}

/**
 * A multisig operation has been cancelled.
 */
export interface MultisigEvent_MultisigCancelled {
    __kind: 'MultisigCancelled'
    cancelling: AccountId32
    timepoint: Timepoint
    multisig: AccountId32
    callHash: Bytes
}

/**
 * A multisig operation has been executed.
 */
export interface MultisigEvent_MultisigExecuted {
    __kind: 'MultisigExecuted'
    approving: AccountId32
    timepoint: Timepoint
    multisig: AccountId32
    callHash: Bytes
    result: Result<null, DispatchError>
}

/**
 * A new multisig operation has begun.
 */
export interface MultisigEvent_NewMultisig {
    __kind: 'NewMultisig'
    approving: AccountId32
    multisig: AccountId32
    callHash: Bytes
}

export interface Timepoint {
    height: number
    index: number
}

/**
 * The `Event` enum of this pallet
 */
export type MembershipEvent = MembershipEvent_MembershipAdded | MembershipEvent_MembershipRemoved | MembershipEvent_MembershipRenewed

/**
 * A membership was added.
 */
export interface MembershipEvent_MembershipAdded {
    __kind: 'MembershipAdded'
    member: number
    expireOn: number
}

/**
 * A membership was removed.
 */
export interface MembershipEvent_MembershipRemoved {
    __kind: 'MembershipRemoved'
    member: number
    reason: MembershipRemovalReason
}

/**
 * A membership was renewed.
 */
export interface MembershipEvent_MembershipRenewed {
    __kind: 'MembershipRenewed'
    member: number
    expireOn: number
}

export type MembershipRemovalReason = MembershipRemovalReason_Expired | MembershipRemovalReason_NotEnoughCerts | MembershipRemovalReason_Revoked | MembershipRemovalReason_System

export interface MembershipRemovalReason_Expired {
    __kind: 'Expired'
}

export interface MembershipRemovalReason_NotEnoughCerts {
    __kind: 'NotEnoughCerts'
}

export interface MembershipRemovalReason_Revoked {
    __kind: 'Revoked'
}

export interface MembershipRemovalReason_System {
    __kind: 'System'
}

/**
 * The `Event` enum of this pallet
 */
export type ImOnlineEvent = ImOnlineEvent_AllGood | ImOnlineEvent_HeartbeatReceived | ImOnlineEvent_SomeOffline

/**
 * At the end of the session, no offence was committed.
 */
export interface ImOnlineEvent_AllGood {
    __kind: 'AllGood'
}

/**
 * A new heartbeat was received from `AuthorityId`.
 */
export interface ImOnlineEvent_HeartbeatReceived {
    __kind: 'HeartbeatReceived'
    authorityId: Bytes
}

/**
 * At the end of the session, at least one validator was found to be offline.
 */
export interface ImOnlineEvent_SomeOffline {
    __kind: 'SomeOffline'
    offline: [AccountId32, ValidatorFullIdentification][]
}

export type ValidatorFullIdentification = null

/**
 * The `Event` enum of this pallet
 */
export type IdentityEvent = IdentityEvent_IdtyChangedOwnerKey | IdentityEvent_IdtyConfirmed | IdentityEvent_IdtyCreated | IdentityEvent_IdtyRemoved | IdentityEvent_IdtyRevoked | IdentityEvent_IdtyValidated

export interface IdentityEvent_IdtyChangedOwnerKey {
    __kind: 'IdtyChangedOwnerKey'
    idtyIndex: number
    newOwnerKey: AccountId32
}

/**
 * An identity has been confirmed by its owner.
 */
export interface IdentityEvent_IdtyConfirmed {
    __kind: 'IdtyConfirmed'
    idtyIndex: number
    ownerKey: AccountId32
    name: IdtyName
}

/**
 * A new identity has been created.
 */
export interface IdentityEvent_IdtyCreated {
    __kind: 'IdtyCreated'
    idtyIndex: number
    ownerKey: AccountId32
}

/**
 * An identity has been removed.
 */
export interface IdentityEvent_IdtyRemoved {
    __kind: 'IdtyRemoved'
    idtyIndex: number
    reason: RemovalReason
}

/**
 * An identity has been revoked.
 */
export interface IdentityEvent_IdtyRevoked {
    __kind: 'IdtyRevoked'
    idtyIndex: number
    reason: RevocationReason
}

/**
 * An identity has been validated.
 */
export interface IdentityEvent_IdtyValidated {
    __kind: 'IdtyValidated'
    idtyIndex: number
}

export type RevocationReason = RevocationReason_Expired | RevocationReason_Root | RevocationReason_User

export interface RevocationReason_Expired {
    __kind: 'Expired'
}

export interface RevocationReason_Root {
    __kind: 'Root'
}

export interface RevocationReason_User {
    __kind: 'User'
}

export type RemovalReason = RemovalReason_Revoked | RemovalReason_Root | RemovalReason_Unconfirmed | RemovalReason_Unvalidated

export interface RemovalReason_Revoked {
    __kind: 'Revoked'
}

export interface RemovalReason_Root {
    __kind: 'Root'
}

export interface RemovalReason_Unconfirmed {
    __kind: 'Unconfirmed'
}

export interface RemovalReason_Unvalidated {
    __kind: 'Unvalidated'
}

export type IdtyName = Bytes

/**
 * The `Event` enum of this pallet
 */
export type GrandpaEvent = GrandpaEvent_NewAuthorities | GrandpaEvent_Paused | GrandpaEvent_Resumed

/**
 * New authority set has been applied.
 */
export interface GrandpaEvent_NewAuthorities {
    __kind: 'NewAuthorities'
    authoritySet: [Public, bigint][]
}

/**
 * Current authority set has been paused.
 */
export interface GrandpaEvent_Paused {
    __kind: 'Paused'
}

/**
 * Current authority set has been resumed.
 */
export interface GrandpaEvent_Resumed {
    __kind: 'Resumed'
}

export type Public = Bytes

/**
 * The `Event` enum of this pallet
 */
export type DistanceEvent = DistanceEvent_EvaluatedInvalid | DistanceEvent_EvaluatedValid | DistanceEvent_EvaluationRequested

/**
 * Distance rule was found invalid.
 */
export interface DistanceEvent_EvaluatedInvalid {
    __kind: 'EvaluatedInvalid'
    idtyIndex: number
    distance: Perbill
}

/**
 * Distance rule was found valid.
 */
export interface DistanceEvent_EvaluatedValid {
    __kind: 'EvaluatedValid'
    idtyIndex: number
    distance: Perbill
}

/**
 * A distance evaluation was requested.
 */
export interface DistanceEvent_EvaluationRequested {
    __kind: 'EvaluationRequested'
    idtyIndex: number
    who: AccountId32
}

export type Perbill = number

/**
 * The `Event` enum of this pallet
 */
export type CertificationEvent = CertificationEvent_CertAdded | CertificationEvent_CertRemoved | CertificationEvent_CertRenewed

/**
 * A new certification was added.
 */
export interface CertificationEvent_CertAdded {
    __kind: 'CertAdded'
    issuer: number
    receiver: number
}

/**
 * A certification was removed.
 */
export interface CertificationEvent_CertRemoved {
    __kind: 'CertRemoved'
    issuer: number
    receiver: number
    expiration: boolean
}

/**
 * A certification was renewed.
 */
export interface CertificationEvent_CertRenewed {
    __kind: 'CertRenewed'
    issuer: number
    receiver: number
}

/**
 * The `Event` enum of this pallet
 */
export type BalancesEvent = BalancesEvent_BalanceSet | BalancesEvent_Burned | BalancesEvent_Deposit | BalancesEvent_DustLost | BalancesEvent_Endowed | BalancesEvent_Frozen | BalancesEvent_Issued | BalancesEvent_Locked | BalancesEvent_Minted | BalancesEvent_Rescinded | BalancesEvent_ReserveRepatriated | BalancesEvent_Reserved | BalancesEvent_Restored | BalancesEvent_Slashed | BalancesEvent_Suspended | BalancesEvent_Thawed | BalancesEvent_TotalIssuanceForced | BalancesEvent_Transfer | BalancesEvent_Unlocked | BalancesEvent_Unreserved | BalancesEvent_Upgraded | BalancesEvent_Withdraw

/**
 * A balance was set by root.
 */
export interface BalancesEvent_BalanceSet {
    __kind: 'BalanceSet'
    who: AccountId32
    free: bigint
}

/**
 * Some amount was burned from an account.
 */
export interface BalancesEvent_Burned {
    __kind: 'Burned'
    who: AccountId32
    amount: bigint
}

/**
 * Some amount was deposited (e.g. for transaction fees).
 */
export interface BalancesEvent_Deposit {
    __kind: 'Deposit'
    who: AccountId32
    amount: bigint
}

/**
 * An account was removed whose balance was non-zero but below ExistentialDeposit,
 * resulting in an outright loss.
 */
export interface BalancesEvent_DustLost {
    __kind: 'DustLost'
    account: AccountId32
    amount: bigint
}

/**
 * An account was created with some free balance.
 */
export interface BalancesEvent_Endowed {
    __kind: 'Endowed'
    account: AccountId32
    freeBalance: bigint
}

/**
 * Some balance was frozen.
 */
export interface BalancesEvent_Frozen {
    __kind: 'Frozen'
    who: AccountId32
    amount: bigint
}

/**
 * Total issuance was increased by `amount`, creating a credit to be balanced.
 */
export interface BalancesEvent_Issued {
    __kind: 'Issued'
    amount: bigint
}

/**
 * Some balance was locked.
 */
export interface BalancesEvent_Locked {
    __kind: 'Locked'
    who: AccountId32
    amount: bigint
}

/**
 * Some amount was minted into an account.
 */
export interface BalancesEvent_Minted {
    __kind: 'Minted'
    who: AccountId32
    amount: bigint
}

/**
 * Total issuance was decreased by `amount`, creating a debt to be balanced.
 */
export interface BalancesEvent_Rescinded {
    __kind: 'Rescinded'
    amount: bigint
}

/**
 * Some balance was moved from the reserve of the first account to the second account.
 * Final argument indicates the destination balance type.
 */
export interface BalancesEvent_ReserveRepatriated {
    __kind: 'ReserveRepatriated'
    from: AccountId32
    to: AccountId32
    amount: bigint
    destinationStatus: BalanceStatus
}

/**
 * Some balance was reserved (moved from free to reserved).
 */
export interface BalancesEvent_Reserved {
    __kind: 'Reserved'
    who: AccountId32
    amount: bigint
}

/**
 * Some amount was restored into an account.
 */
export interface BalancesEvent_Restored {
    __kind: 'Restored'
    who: AccountId32
    amount: bigint
}

/**
 * Some amount was removed from the account (e.g. for misbehavior).
 */
export interface BalancesEvent_Slashed {
    __kind: 'Slashed'
    who: AccountId32
    amount: bigint
}

/**
 * Some amount was suspended from an account (it can be restored later).
 */
export interface BalancesEvent_Suspended {
    __kind: 'Suspended'
    who: AccountId32
    amount: bigint
}

/**
 * Some balance was thawed.
 */
export interface BalancesEvent_Thawed {
    __kind: 'Thawed'
    who: AccountId32
    amount: bigint
}

/**
 * The `TotalIssuance` was forcefully changed.
 */
export interface BalancesEvent_TotalIssuanceForced {
    __kind: 'TotalIssuanceForced'
    old: bigint
    new: bigint
}

/**
 * Transfer succeeded.
 */
export interface BalancesEvent_Transfer {
    __kind: 'Transfer'
    from: AccountId32
    to: AccountId32
    amount: bigint
}

/**
 * Some balance was unlocked.
 */
export interface BalancesEvent_Unlocked {
    __kind: 'Unlocked'
    who: AccountId32
    amount: bigint
}

/**
 * Some balance was unreserved (moved from reserved to free).
 */
export interface BalancesEvent_Unreserved {
    __kind: 'Unreserved'
    who: AccountId32
    amount: bigint
}

/**
 * An account was upgraded.
 */
export interface BalancesEvent_Upgraded {
    __kind: 'Upgraded'
    who: AccountId32
}

/**
 * Some amount was withdrawn from the account (e.g. for transaction fees).
 */
export interface BalancesEvent_Withdraw {
    __kind: 'Withdraw'
    who: AccountId32
    amount: bigint
}

export type BalanceStatus = BalanceStatus_Free | BalanceStatus_Reserved

export interface BalanceStatus_Free {
    __kind: 'Free'
}

export interface BalanceStatus_Reserved {
    __kind: 'Reserved'
}

/**
 * The `Event` enum of this pallet
 */
export type AuthorityMembersEvent = AuthorityMembersEvent_IncomingAuthorities | AuthorityMembersEvent_MemberAddedToBlacklist | AuthorityMembersEvent_MemberGoOffline | AuthorityMembersEvent_MemberGoOnline | AuthorityMembersEvent_MemberRemoved | AuthorityMembersEvent_MemberRemovedFromBlacklist | AuthorityMembersEvent_OutgoingAuthorities

/**
 * List of members scheduled to join the set of authorities in the next session.
 */
export interface AuthorityMembersEvent_IncomingAuthorities {
    __kind: 'IncomingAuthorities'
    members: number[]
}

/**
 * A member has been blacklisted.
 */
export interface AuthorityMembersEvent_MemberAddedToBlacklist {
    __kind: 'MemberAddedToBlacklist'
    member: number
}

/**
 * A member will leave the set of authorities in 2 sessions.
 */
export interface AuthorityMembersEvent_MemberGoOffline {
    __kind: 'MemberGoOffline'
    member: number
}

/**
 * A member will join the set of authorities in 2 sessions.
 */
export interface AuthorityMembersEvent_MemberGoOnline {
    __kind: 'MemberGoOnline'
    member: number
}

/**
 * A member, who no longer has authority rights, will be removed from the authority set in 2 sessions.
 */
export interface AuthorityMembersEvent_MemberRemoved {
    __kind: 'MemberRemoved'
    member: number
}

/**
 * A member has been removed from the blacklist.
 */
export interface AuthorityMembersEvent_MemberRemovedFromBlacklist {
    __kind: 'MemberRemovedFromBlacklist'
    member: number
}

/**
 * List of members leaving the set of authorities in the next session.
 */
export interface AuthorityMembersEvent_OutgoingAuthorities {
    __kind: 'OutgoingAuthorities'
    members: number[]
}

/**
 * Event of atomic swap pallet.
 */
export type AtomicSwapEvent = AtomicSwapEvent_NewSwap | AtomicSwapEvent_SwapCancelled | AtomicSwapEvent_SwapClaimed

/**
 * Swap created.
 */
export interface AtomicSwapEvent_NewSwap {
    __kind: 'NewSwap'
    account: AccountId32
    proof: Bytes
    swap: PendingSwap
}

/**
 * Swap cancelled.
 */
export interface AtomicSwapEvent_SwapCancelled {
    __kind: 'SwapCancelled'
    account: AccountId32
    proof: Bytes
}

/**
 * Swap claimed. The last parameter indicates whether the execution succeeds.
 */
export interface AtomicSwapEvent_SwapClaimed {
    __kind: 'SwapClaimed'
    account: AccountId32
    proof: Bytes
    success: boolean
}

export interface PendingSwap {
    source: AccountId32
    action: BalanceSwapAction
    endBlock: number
}

export interface BalanceSwapAction {
    value: bigint
}

/**
 * The `Event` enum of this pallet
 */
export type AccountEvent = AccountEvent_AccountLinked | AccountEvent_AccountUnlinked

/**
 * account linked to identity
 */
export interface AccountEvent_AccountLinked {
    __kind: 'AccountLinked'
    who: AccountId32
    identity: number
}

/**
 * The account was unlinked from its identity.
 */
export interface AccountEvent_AccountUnlinked {
    __kind: 'AccountUnlinked'
    value: AccountId32
}

export type Phase = Phase_ApplyExtrinsic | Phase_Finalization | Phase_Initialization

export interface Phase_ApplyExtrinsic {
    __kind: 'ApplyExtrinsic'
    value: number
}

export interface Phase_Finalization {
    __kind: 'Finalization'
}

export interface Phase_Initialization {
    __kind: 'Initialization'
}

export const EventRecord: sts.Type<EventRecord> = sts.struct(() => {
    return  {
        phase: Phase,
        event: Event,
        topics: sts.array(() => H256),
    }
})

export const H256 = sts.bytes()

export const Event: sts.Type<Event> = sts.closedEnum(() => {
    return  {
        Account: AccountEvent,
        AtomicSwap: AtomicSwapEvent,
        AuthorityMembers: AuthorityMembersEvent,
        Balances: BalancesEvent,
        Certification: CertificationEvent,
        Distance: DistanceEvent,
        Grandpa: GrandpaEvent,
        Identity: IdentityEvent,
        ImOnline: ImOnlineEvent,
        Membership: MembershipEvent,
        Multisig: MultisigEvent,
        Offences: OffencesEvent,
        OneshotAccount: OneshotAccountEvent,
        Preimage: PreimageEvent,
        ProvideRandomness: ProvideRandomnessEvent,
        Proxy: ProxyEvent,
        Quota: QuotaEvent,
        Scheduler: SchedulerEvent,
        Session: SessionEvent,
        SmithMembers: SmithMembersEvent,
        Sudo: SudoEvent,
        System: SystemEvent,
        TechnicalCommittee: TechnicalCommitteeEvent,
        TransactionPayment: TransactionPaymentEvent,
        Treasury: TreasuryEvent,
        UniversalDividend: UniversalDividendEvent,
        UpgradeOrigin: UpgradeOriginEvent,
        Utility: UtilityEvent,
    }
})

/**
 * The `Event` enum of this pallet
 */
export const UtilityEvent: sts.Type<UtilityEvent> = sts.closedEnum(() => {
    return  {
        BatchCompleted: sts.unit(),
        BatchCompletedWithErrors: sts.unit(),
        BatchInterrupted: sts.enumStruct({
            index: sts.number(),
            error: DispatchError,
        }),
        DispatchedAs: sts.enumStruct({
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        ItemCompleted: sts.unit(),
        ItemFailed: sts.enumStruct({
            error: DispatchError,
        }),
    }
})

export const DispatchError: sts.Type<DispatchError> = sts.closedEnum(() => {
    return  {
        Arithmetic: ArithmeticError,
        BadOrigin: sts.unit(),
        CannotLookup: sts.unit(),
        ConsumerRemaining: sts.unit(),
        Corruption: sts.unit(),
        Exhausted: sts.unit(),
        Module: ModuleError,
        NoProviders: sts.unit(),
        Other: sts.unit(),
        RootNotAllowed: sts.unit(),
        Token: TokenError,
        TooManyConsumers: sts.unit(),
        Transactional: TransactionalError,
        Unavailable: sts.unit(),
    }
})

export const TransactionalError: sts.Type<TransactionalError> = sts.closedEnum(() => {
    return  {
        LimitReached: sts.unit(),
        NoLayer: sts.unit(),
    }
})

export const TokenError: sts.Type<TokenError> = sts.closedEnum(() => {
    return  {
        BelowMinimum: sts.unit(),
        Blocked: sts.unit(),
        CannotCreate: sts.unit(),
        CannotCreateHold: sts.unit(),
        Frozen: sts.unit(),
        FundsUnavailable: sts.unit(),
        NotExpendable: sts.unit(),
        OnlyProvider: sts.unit(),
        UnknownAsset: sts.unit(),
        Unsupported: sts.unit(),
    }
})

export const ModuleError: sts.Type<ModuleError> = sts.struct(() => {
    return  {
        index: sts.number(),
        error: sts.bytes(),
    }
})

export const ArithmeticError: sts.Type<ArithmeticError> = sts.closedEnum(() => {
    return  {
        DivisionByZero: sts.unit(),
        Overflow: sts.unit(),
        Underflow: sts.unit(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const UpgradeOriginEvent: sts.Type<UpgradeOriginEvent> = sts.closedEnum(() => {
    return  {
        DispatchedAsRoot: sts.enumStruct({
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const UniversalDividendEvent: sts.Type<UniversalDividendEvent> = sts.closedEnum(() => {
    return  {
        NewUdCreated: sts.enumStruct({
            amount: sts.bigint(),
            index: sts.number(),
            monetaryMass: sts.bigint(),
            membersCount: sts.bigint(),
        }),
        UdReevalued: sts.enumStruct({
            newUdAmount: sts.bigint(),
            monetaryMass: sts.bigint(),
            membersCount: sts.bigint(),
        }),
        UdsAutoPaid: sts.enumStruct({
            count: sts.number(),
            total: sts.bigint(),
            who: AccountId32,
        }),
        UdsClaimed: sts.enumStruct({
            count: sts.number(),
            total: sts.bigint(),
            who: AccountId32,
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const TreasuryEvent: sts.Type<TreasuryEvent> = sts.closedEnum(() => {
    return  {
        AssetSpendApproved: sts.enumStruct({
            index: sts.number(),
            amount: sts.bigint(),
            beneficiary: AccountId32,
            validFrom: sts.number(),
            expireAt: sts.number(),
        }),
        AssetSpendVoided: sts.enumStruct({
            index: sts.number(),
        }),
        Awarded: sts.enumStruct({
            proposalIndex: sts.number(),
            award: sts.bigint(),
            account: AccountId32,
        }),
        Burnt: sts.enumStruct({
            burntFunds: sts.bigint(),
        }),
        Deposit: sts.enumStruct({
            value: sts.bigint(),
        }),
        Paid: sts.enumStruct({
            index: sts.number(),
        }),
        PaymentFailed: sts.enumStruct({
            index: sts.number(),
        }),
        Rollover: sts.enumStruct({
            rolloverBalance: sts.bigint(),
        }),
        SpendApproved: sts.enumStruct({
            proposalIndex: sts.number(),
            amount: sts.bigint(),
            beneficiary: AccountId32,
        }),
        SpendProcessed: sts.enumStruct({
            index: sts.number(),
        }),
        Spending: sts.enumStruct({
            budgetRemaining: sts.bigint(),
        }),
        UpdatedInactive: sts.enumStruct({
            reactivated: sts.bigint(),
            deactivated: sts.bigint(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const TransactionPaymentEvent: sts.Type<TransactionPaymentEvent> = sts.closedEnum(() => {
    return  {
        TransactionFeePaid: sts.enumStruct({
            who: AccountId32,
            actualFee: sts.bigint(),
            tip: sts.bigint(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const TechnicalCommitteeEvent: sts.Type<TechnicalCommitteeEvent> = sts.closedEnum(() => {
    return  {
        Approved: sts.enumStruct({
            proposalHash: H256,
        }),
        Closed: sts.enumStruct({
            proposalHash: H256,
            yes: sts.number(),
            no: sts.number(),
        }),
        Disapproved: sts.enumStruct({
            proposalHash: H256,
        }),
        Executed: sts.enumStruct({
            proposalHash: H256,
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        MemberExecuted: sts.enumStruct({
            proposalHash: H256,
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        Proposed: sts.enumStruct({
            account: AccountId32,
            proposalIndex: sts.number(),
            proposalHash: H256,
            threshold: sts.number(),
        }),
        Voted: sts.enumStruct({
            account: AccountId32,
            proposalHash: H256,
            voted: sts.boolean(),
            yes: sts.number(),
            no: sts.number(),
        }),
    }
})

/**
 * Event for the System pallet.
 */
export const SystemEvent: sts.Type<SystemEvent> = sts.closedEnum(() => {
    return  {
        CodeUpdated: sts.unit(),
        ExtrinsicFailed: sts.enumStruct({
            dispatchError: DispatchError,
            dispatchInfo: DispatchInfo,
        }),
        ExtrinsicSuccess: sts.enumStruct({
            dispatchInfo: DispatchInfo,
        }),
        KilledAccount: sts.enumStruct({
            account: AccountId32,
        }),
        NewAccount: sts.enumStruct({
            account: AccountId32,
        }),
        Remarked: sts.enumStruct({
            sender: AccountId32,
            hash: H256,
        }),
        UpgradeAuthorized: sts.enumStruct({
            codeHash: H256,
            checkVersion: sts.boolean(),
        }),
    }
})

export const DispatchInfo: sts.Type<DispatchInfo> = sts.struct(() => {
    return  {
        weight: Weight,
        class: DispatchClass,
        paysFee: Pays,
    }
})

export const Pays: sts.Type<Pays> = sts.closedEnum(() => {
    return  {
        No: sts.unit(),
        Yes: sts.unit(),
    }
})

export const DispatchClass: sts.Type<DispatchClass> = sts.closedEnum(() => {
    return  {
        Mandatory: sts.unit(),
        Normal: sts.unit(),
        Operational: sts.unit(),
    }
})

export const Weight: sts.Type<Weight> = sts.struct(() => {
    return  {
        refTime: sts.bigint(),
        proofSize: sts.bigint(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const SudoEvent: sts.Type<SudoEvent> = sts.closedEnum(() => {
    return  {
        KeyChanged: sts.enumStruct({
            old: sts.option(() => AccountId32),
            new: AccountId32,
        }),
        KeyRemoved: sts.unit(),
        Sudid: sts.enumStruct({
            sudoResult: sts.result(() => sts.unit(), () => DispatchError),
        }),
        SudoAsDone: sts.enumStruct({
            sudoResult: sts.result(() => sts.unit(), () => DispatchError),
        }),
    }
})

/**
 * Events type.
 */
export const SmithMembersEvent: sts.Type<SmithMembersEvent> = sts.closedEnum(() => {
    return  {
        InvitationAccepted: sts.enumStruct({
            idtyIndex: sts.number(),
        }),
        InvitationSent: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
        }),
        SmithCertAdded: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
        }),
        SmithCertRemoved: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
        }),
        SmithMembershipAdded: sts.enumStruct({
            idtyIndex: sts.number(),
        }),
        SmithMembershipRemoved: sts.enumStruct({
            idtyIndex: sts.number(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const SessionEvent: sts.Type<SessionEvent> = sts.closedEnum(() => {
    return  {
        NewSession: sts.enumStruct({
            sessionIndex: sts.number(),
        }),
    }
})

/**
 * Events type.
 */
export const SchedulerEvent: sts.Type<SchedulerEvent> = sts.closedEnum(() => {
    return  {
        CallUnavailable: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
        }),
        Canceled: sts.enumStruct({
            when: sts.number(),
            index: sts.number(),
        }),
        Dispatched: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        PeriodicFailed: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
        }),
        PermanentlyOverweight: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
        }),
        RetryCancelled: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
        }),
        RetryFailed: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
        }),
        RetrySet: sts.enumStruct({
            task: sts.tuple(() => [sts.number(), sts.number()]),
            id: sts.option(() => sts.bytes()),
            period: sts.number(),
            retries: sts.number(),
        }),
        Scheduled: sts.enumStruct({
            when: sts.number(),
            index: sts.number(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const QuotaEvent: sts.Type<QuotaEvent> = sts.closedEnum(() => {
    return  {
        NoMoreCurrencyForRefund: sts.unit(),
        NoQuotaForIdty: sts.number(),
        RefundFailed: AccountId32,
        RefundQueueFull: sts.unit(),
        Refunded: sts.enumStruct({
            who: AccountId32,
            identity: sts.number(),
            amount: sts.bigint(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const ProxyEvent: sts.Type<ProxyEvent> = sts.closedEnum(() => {
    return  {
        Announced: sts.enumStruct({
            real: AccountId32,
            proxy: AccountId32,
            callHash: H256,
        }),
        ProxyAdded: sts.enumStruct({
            delegator: AccountId32,
            delegatee: AccountId32,
            proxyType: ProxyType,
            delay: sts.number(),
        }),
        ProxyExecuted: sts.enumStruct({
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        ProxyRemoved: sts.enumStruct({
            delegator: AccountId32,
            delegatee: AccountId32,
            proxyType: ProxyType,
            delay: sts.number(),
        }),
        PureCreated: sts.enumStruct({
            pure: AccountId32,
            who: AccountId32,
            proxyType: ProxyType,
            disambiguationIndex: sts.number(),
        }),
    }
})

export const ProxyType: sts.Type<ProxyType> = sts.closedEnum(() => {
    return  {
        AlmostAny: sts.unit(),
        CancelProxy: sts.unit(),
        TechnicalCommitteePropose: sts.unit(),
        TransferOnly: sts.unit(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const ProvideRandomnessEvent: sts.Type<ProvideRandomnessEvent> = sts.closedEnum(() => {
    return  {
        FilledRandomness: sts.enumStruct({
            requestId: sts.bigint(),
            randomness: H256,
        }),
        RequestedRandomness: sts.enumStruct({
            requestId: sts.bigint(),
            salt: H256,
            type: RandomnessType,
        }),
    }
})

export const RandomnessType: sts.Type<RandomnessType> = sts.closedEnum(() => {
    return  {
        RandomnessFromOneEpochAgo: sts.unit(),
        RandomnessFromPreviousBlock: sts.unit(),
        RandomnessFromTwoEpochsAgo: sts.unit(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const PreimageEvent: sts.Type<PreimageEvent> = sts.closedEnum(() => {
    return  {
        Cleared: sts.enumStruct({
            hash: H256,
        }),
        Noted: sts.enumStruct({
            hash: H256,
        }),
        Requested: sts.enumStruct({
            hash: H256,
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const OneshotAccountEvent: sts.Type<OneshotAccountEvent> = sts.closedEnum(() => {
    return  {
        OneshotAccountConsumed: sts.enumStruct({
            account: AccountId32,
            dest1: sts.tuple(() => [AccountId32, sts.bigint()]),
            dest2: sts.option(() => sts.tuple(() => [AccountId32, sts.bigint()])),
        }),
        OneshotAccountCreated: sts.enumStruct({
            account: AccountId32,
            balance: sts.bigint(),
            creator: AccountId32,
        }),
        Withdraw: sts.enumStruct({
            account: AccountId32,
            balance: sts.bigint(),
        }),
    }
})

/**
 * Events type.
 */
export const OffencesEvent: sts.Type<OffencesEvent> = sts.closedEnum(() => {
    return  {
        Offence: sts.enumStruct({
            kind: sts.bytes(),
            timeslot: sts.bytes(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const MultisigEvent: sts.Type<MultisigEvent> = sts.closedEnum(() => {
    return  {
        MultisigApproval: sts.enumStruct({
            approving: AccountId32,
            timepoint: Timepoint,
            multisig: AccountId32,
            callHash: sts.bytes(),
        }),
        MultisigCancelled: sts.enumStruct({
            cancelling: AccountId32,
            timepoint: Timepoint,
            multisig: AccountId32,
            callHash: sts.bytes(),
        }),
        MultisigExecuted: sts.enumStruct({
            approving: AccountId32,
            timepoint: Timepoint,
            multisig: AccountId32,
            callHash: sts.bytes(),
            result: sts.result(() => sts.unit(), () => DispatchError),
        }),
        NewMultisig: sts.enumStruct({
            approving: AccountId32,
            multisig: AccountId32,
            callHash: sts.bytes(),
        }),
    }
})

export const Timepoint: sts.Type<Timepoint> = sts.struct(() => {
    return  {
        height: sts.number(),
        index: sts.number(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const MembershipEvent: sts.Type<MembershipEvent> = sts.closedEnum(() => {
    return  {
        MembershipAdded: sts.enumStruct({
            member: sts.number(),
            expireOn: sts.number(),
        }),
        MembershipRemoved: sts.enumStruct({
            member: sts.number(),
            reason: MembershipRemovalReason,
        }),
        MembershipRenewed: sts.enumStruct({
            member: sts.number(),
            expireOn: sts.number(),
        }),
    }
})

export const MembershipRemovalReason: sts.Type<MembershipRemovalReason> = sts.closedEnum(() => {
    return  {
        Expired: sts.unit(),
        NotEnoughCerts: sts.unit(),
        Revoked: sts.unit(),
        System: sts.unit(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const ImOnlineEvent: sts.Type<ImOnlineEvent> = sts.closedEnum(() => {
    return  {
        AllGood: sts.unit(),
        HeartbeatReceived: sts.enumStruct({
            authorityId: sts.bytes(),
        }),
        SomeOffline: sts.enumStruct({
            offline: sts.array(() => sts.tuple(() => [AccountId32, ValidatorFullIdentification])),
        }),
    }
})

export const ValidatorFullIdentification = sts.unit()

/**
 * The `Event` enum of this pallet
 */
export const IdentityEvent: sts.Type<IdentityEvent> = sts.closedEnum(() => {
    return  {
        IdtyChangedOwnerKey: sts.enumStruct({
            idtyIndex: sts.number(),
            newOwnerKey: AccountId32,
        }),
        IdtyConfirmed: sts.enumStruct({
            idtyIndex: sts.number(),
            ownerKey: AccountId32,
            name: IdtyName,
        }),
        IdtyCreated: sts.enumStruct({
            idtyIndex: sts.number(),
            ownerKey: AccountId32,
        }),
        IdtyRemoved: sts.enumStruct({
            idtyIndex: sts.number(),
            reason: RemovalReason,
        }),
        IdtyRevoked: sts.enumStruct({
            idtyIndex: sts.number(),
            reason: RevocationReason,
        }),
        IdtyValidated: sts.enumStruct({
            idtyIndex: sts.number(),
        }),
    }
})

export const RevocationReason: sts.Type<RevocationReason> = sts.closedEnum(() => {
    return  {
        Expired: sts.unit(),
        Root: sts.unit(),
        User: sts.unit(),
    }
})

export const RemovalReason: sts.Type<RemovalReason> = sts.closedEnum(() => {
    return  {
        Revoked: sts.unit(),
        Root: sts.unit(),
        Unconfirmed: sts.unit(),
        Unvalidated: sts.unit(),
    }
})

export const IdtyName = sts.bytes()

/**
 * The `Event` enum of this pallet
 */
export const GrandpaEvent: sts.Type<GrandpaEvent> = sts.closedEnum(() => {
    return  {
        NewAuthorities: sts.enumStruct({
            authoritySet: sts.array(() => sts.tuple(() => [Public, sts.bigint()])),
        }),
        Paused: sts.unit(),
        Resumed: sts.unit(),
    }
})

export const Public = sts.bytes()

/**
 * The `Event` enum of this pallet
 */
export const DistanceEvent: sts.Type<DistanceEvent> = sts.closedEnum(() => {
    return  {
        EvaluatedInvalid: sts.enumStruct({
            idtyIndex: sts.number(),
            distance: Perbill,
        }),
        EvaluatedValid: sts.enumStruct({
            idtyIndex: sts.number(),
            distance: Perbill,
        }),
        EvaluationRequested: sts.enumStruct({
            idtyIndex: sts.number(),
            who: AccountId32,
        }),
    }
})

export const Perbill = sts.number()

/**
 * The `Event` enum of this pallet
 */
export const CertificationEvent: sts.Type<CertificationEvent> = sts.closedEnum(() => {
    return  {
        CertAdded: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
        }),
        CertRemoved: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
            expiration: sts.boolean(),
        }),
        CertRenewed: sts.enumStruct({
            issuer: sts.number(),
            receiver: sts.number(),
        }),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const BalancesEvent: sts.Type<BalancesEvent> = sts.closedEnum(() => {
    return  {
        BalanceSet: sts.enumStruct({
            who: AccountId32,
            free: sts.bigint(),
        }),
        Burned: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Deposit: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        DustLost: sts.enumStruct({
            account: AccountId32,
            amount: sts.bigint(),
        }),
        Endowed: sts.enumStruct({
            account: AccountId32,
            freeBalance: sts.bigint(),
        }),
        Frozen: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Issued: sts.enumStruct({
            amount: sts.bigint(),
        }),
        Locked: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Minted: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Rescinded: sts.enumStruct({
            amount: sts.bigint(),
        }),
        ReserveRepatriated: sts.enumStruct({
            from: AccountId32,
            to: AccountId32,
            amount: sts.bigint(),
            destinationStatus: BalanceStatus,
        }),
        Reserved: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Restored: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Slashed: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Suspended: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Thawed: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        TotalIssuanceForced: sts.enumStruct({
            old: sts.bigint(),
            new: sts.bigint(),
        }),
        Transfer: sts.enumStruct({
            from: AccountId32,
            to: AccountId32,
            amount: sts.bigint(),
        }),
        Unlocked: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Unreserved: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
        Upgraded: sts.enumStruct({
            who: AccountId32,
        }),
        Withdraw: sts.enumStruct({
            who: AccountId32,
            amount: sts.bigint(),
        }),
    }
})

export const BalanceStatus: sts.Type<BalanceStatus> = sts.closedEnum(() => {
    return  {
        Free: sts.unit(),
        Reserved: sts.unit(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const AuthorityMembersEvent: sts.Type<AuthorityMembersEvent> = sts.closedEnum(() => {
    return  {
        IncomingAuthorities: sts.enumStruct({
            members: sts.array(() => sts.number()),
        }),
        MemberAddedToBlacklist: sts.enumStruct({
            member: sts.number(),
        }),
        MemberGoOffline: sts.enumStruct({
            member: sts.number(),
        }),
        MemberGoOnline: sts.enumStruct({
            member: sts.number(),
        }),
        MemberRemoved: sts.enumStruct({
            member: sts.number(),
        }),
        MemberRemovedFromBlacklist: sts.enumStruct({
            member: sts.number(),
        }),
        OutgoingAuthorities: sts.enumStruct({
            members: sts.array(() => sts.number()),
        }),
    }
})

/**
 * Event of atomic swap pallet.
 */
export const AtomicSwapEvent: sts.Type<AtomicSwapEvent> = sts.closedEnum(() => {
    return  {
        NewSwap: sts.enumStruct({
            account: AccountId32,
            proof: sts.bytes(),
            swap: PendingSwap,
        }),
        SwapCancelled: sts.enumStruct({
            account: AccountId32,
            proof: sts.bytes(),
        }),
        SwapClaimed: sts.enumStruct({
            account: AccountId32,
            proof: sts.bytes(),
            success: sts.boolean(),
        }),
    }
})

export const PendingSwap: sts.Type<PendingSwap> = sts.struct(() => {
    return  {
        source: AccountId32,
        action: BalanceSwapAction,
        endBlock: sts.number(),
    }
})

export const BalanceSwapAction: sts.Type<BalanceSwapAction> = sts.struct(() => {
    return  {
        value: sts.bigint(),
    }
})

/**
 * The `Event` enum of this pallet
 */
export const AccountEvent: sts.Type<AccountEvent> = sts.closedEnum(() => {
    return  {
        AccountLinked: sts.enumStruct({
            who: AccountId32,
            identity: sts.number(),
        }),
        AccountUnlinked: AccountId32,
    }
})

export const Phase: sts.Type<Phase> = sts.closedEnum(() => {
    return  {
        ApplyExtrinsic: sts.number(),
        Finalization: sts.unit(),
        Initialization: sts.unit(),
    }
})

export const AdjustmentDirection: sts.Type<AdjustmentDirection> = sts.closedEnum(() => {
    return  {
        Decrease: sts.unit(),
        Increase: sts.unit(),
    }
})

export type AdjustmentDirection = AdjustmentDirection_Decrease | AdjustmentDirection_Increase

export interface AdjustmentDirection_Decrease {
    __kind: 'Decrease'
}

export interface AdjustmentDirection_Increase {
    __kind: 'Increase'
}
