import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const idtyQuota =  {
    /**
     *  maps identity index to quota
     */
    v800: new StorageType('Quota.IdtyQuota', 'Optional', [sts.number()], v800.Quota) as IdtyQuotaV800,
}

/**
 *  maps identity index to quota
 */
export interface IdtyQuotaV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<(v800.Quota | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.Quota | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.Quota | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.Quota | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.Quota | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.Quota | undefined)][]>
}

export const refundQueue =  {
    /**
     *  fees waiting for refund
     */
    v800: new StorageType('Quota.RefundQueue', 'Default', [], sts.array(() => v800.Refund)) as RefundQueueV800,
}

/**
 *  fees waiting for refund
 */
export interface RefundQueueV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.Refund[]
    get(block: Block): Promise<(v800.Refund[] | undefined)>
}
