import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const refunded =  {
    name: 'Quota.Refunded',
    /**
     * Transaction fees were refunded.
     */
    v800: new EventType(
        'Quota.Refunded',
        sts.struct({
            who: v800.AccountId32,
            identity: sts.number(),
            amount: sts.bigint(),
        })
    ),
}

export const noQuotaForIdty =  {
    name: 'Quota.NoQuotaForIdty',
    /**
     * No more quota available for refund.
     */
    v800: new EventType(
        'Quota.NoQuotaForIdty',
        sts.number()
    ),
}

export const noMoreCurrencyForRefund =  {
    name: 'Quota.NoMoreCurrencyForRefund',
    /**
     * No more currency available for refund.
     * This scenario should never occur if the fees are intended for the refund account.
     */
    v800: new EventType(
        'Quota.NoMoreCurrencyForRefund',
        sts.unit()
    ),
}

export const refundFailed =  {
    name: 'Quota.RefundFailed',
    /**
     * The refund has failed.
     * This scenario should rarely occur, except when the account was destroyed in the interim between the request and the refund.
     */
    v800: new EventType(
        'Quota.RefundFailed',
        v800.AccountId32
    ),
}

export const refundQueueFull =  {
    name: 'Quota.RefundQueueFull',
    /**
     * Refund queue was full.
     */
    v800: new EventType(
        'Quota.RefundQueueFull',
        sts.unit()
    ),
}
