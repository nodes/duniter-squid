import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const maxPastReeval =  {
    /**
     *  Maximum number of past UD revaluations to keep in storage.
     */
    v800: new ConstantType(
        'UniversalDividend.MaxPastReeval',
        sts.number()
    ),
}

export const squareMoneyGrowthRate =  {
    /**
     *  Square of the money growth rate per ud reevaluation period
     */
    v800: new ConstantType(
        'UniversalDividend.SquareMoneyGrowthRate',
        v800.Perbill
    ),
}

export const udCreationPeriod =  {
    /**
     *  Universal dividend creation period (ms)
     */
    v800: new ConstantType(
        'UniversalDividend.UdCreationPeriod',
        sts.bigint()
    ),
}

export const udReevalPeriod =  {
    /**
     *  Universal dividend reevaluation period (ms)
     */
    v800: new ConstantType(
        'UniversalDividend.UdReevalPeriod',
        sts.bigint()
    ),
}

export const unitsPerUd =  {
    /**
     *  The number of units to divide the amounts expressed in number of UDs
     *  Example: If you wish to express the UD amounts with a maximum precision of the order
     *  of the milliUD, choose 1000
     */
    v800: new ConstantType(
        'UniversalDividend.UnitsPerUd',
        sts.bigint()
    ),
}
