import {sts, Block, Bytes, Option, Result, ConstantType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const blockWeights =  {
    /**
     *  Block & extrinsics weights: base values and limits.
     */
    v800: new ConstantType(
        'System.BlockWeights',
        v800.BlockWeights
    ),
}

export const blockLength =  {
    /**
     *  The maximum length of a block (in bytes).
     */
    v800: new ConstantType(
        'System.BlockLength',
        v800.BlockLength
    ),
}

export const blockHashCount =  {
    /**
     *  Maximum number of block number to block hash mappings to keep (oldest pruned first).
     */
    v800: new ConstantType(
        'System.BlockHashCount',
        sts.number()
    ),
}

export const dbWeight =  {
    /**
     *  The weight of runtime database operations the runtime can invoke.
     */
    v800: new ConstantType(
        'System.DbWeight',
        v800.RuntimeDbWeight
    ),
}

export const version =  {
    /**
     *  Get the chain's current version.
     */
    v800: new ConstantType(
        'System.Version',
        v800.RuntimeVersion
    ),
}

export const ss58Prefix =  {
    /**
     *  The designated SS58 prefix of this chain.
     * 
     *  This replaces the "ss58Format" property declared in the chain spec. Reason is
     *  that the runtime should know about the prefix in order to make use of it as
     *  an identifier of the chain.
     */
    v800: new ConstantType(
        'System.SS58Prefix',
        sts.number()
    ),
}
