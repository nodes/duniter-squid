import {sts, Block, Bytes, Option, Result, StorageType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const storageIdtyCertMeta =  {
    /**
     *  Certifications metada by issuer.
     */
    v800: new StorageType('Certification.StorageIdtyCertMeta', 'Default', [sts.number()], v800.IdtyCertMeta) as StorageIdtyCertMetaV800,
}

/**
 *  Certifications metada by issuer.
 */
export interface StorageIdtyCertMetaV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): v800.IdtyCertMeta
    get(block: Block, key: number): Promise<(v800.IdtyCertMeta | undefined)>
    getMany(block: Block, keys: number[]): Promise<(v800.IdtyCertMeta | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: (v800.IdtyCertMeta | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: (v800.IdtyCertMeta | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: (v800.IdtyCertMeta | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: (v800.IdtyCertMeta | undefined)][]>
}

export const certsByReceiver =  {
    /**
     *  Certifications by receiver.
     */
    v800: new StorageType('Certification.CertsByReceiver', 'Default', [sts.number()], sts.array(() => sts.tuple(() => [sts.number(), sts.number()]))) as CertsByReceiverV800,
}

/**
 *  Certifications by receiver.
 */
export interface CertsByReceiverV800  {
    is(block: RuntimeCtx): boolean
    getDefault(block: Block): [number, number][]
    get(block: Block, key: number): Promise<([number, number][] | undefined)>
    getMany(block: Block, keys: number[]): Promise<([number, number][] | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: ([number, number][] | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: ([number, number][] | undefined)][]>
}

export const certsRemovableOn =  {
    /**
     *  Certifications removable on.
     */
    v800: new StorageType('Certification.CertsRemovableOn', 'Optional', [sts.number()], sts.array(() => sts.tuple(() => [sts.number(), sts.number()]))) as CertsRemovableOnV800,
}

/**
 *  Certifications removable on.
 */
export interface CertsRemovableOnV800  {
    is(block: RuntimeCtx): boolean
    get(block: Block, key: number): Promise<([number, number][] | undefined)>
    getMany(block: Block, keys: number[]): Promise<([number, number][] | undefined)[]>
    getKeys(block: Block): Promise<number[]>
    getKeys(block: Block, key: number): Promise<number[]>
    getKeysPaged(pageSize: number, block: Block): AsyncIterable<number[]>
    getKeysPaged(pageSize: number, block: Block, key: number): AsyncIterable<number[]>
    getPairs(block: Block): Promise<[k: number, v: ([number, number][] | undefined)][]>
    getPairs(block: Block, key: number): Promise<[k: number, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block): AsyncIterable<[k: number, v: ([number, number][] | undefined)][]>
    getPairsPaged(pageSize: number, block: Block, key: number): AsyncIterable<[k: number, v: ([number, number][] | undefined)][]>
}
