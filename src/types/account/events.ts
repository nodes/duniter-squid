import {sts, Block, Bytes, Option, Result, EventType, RuntimeCtx} from '../support'
import * as v800 from '../v800'

export const accountLinked =  {
    name: 'Account.AccountLinked',
    /**
     * account linked to identity
     */
    v800: new EventType(
        'Account.AccountLinked',
        sts.struct({
            who: v800.AccountId32,
            identity: sts.number(),
        })
    ),
}

export const accountUnlinked =  {
    name: 'Account.AccountUnlinked',
    /**
     * The account was unlinked from its identity.
     */
    v800: new EventType(
        'Account.AccountUnlinked',
        v800.AccountId32
    ),
}
