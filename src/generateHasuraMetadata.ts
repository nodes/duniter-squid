import { loadModel } from "@subsquid/openreader/lib/tools";
import fs from 'fs';
import yaml from 'js-yaml';
import { camelToSnakeCase, ensureDirectoryExists } from "./utils";
import { ArrayRelationship, EntityProperties, HasuraTable, LookupRelationship, ObjectRelationship } from "./types_custom";
import { EnumMigrator } from "./enum_migrator";

const schemaFile = "schema.graphql";
const metadataPath = "./hasura/metadata";
const metadataDbPath = `${metadataPath}/databases/default`;

/**
 * Main function to generate Hasura metadata from a GraphQL schema.
 */
function generateMetadata() {
    const schema = loadModel(schemaFile);
    const tablesDirPath = `${metadataDbPath}/tables`;
    ensureDirectoryExists(tablesDirPath);
    const enumMigrations = new EnumMigrator();
    const tableIncludes: string[] = [];

    for (let [tableNameCamel, details] of Object.entries(schema)) {
        let tableNameSnake = camelToSnakeCase(tableNameCamel);
        let kind = Object.entries(details)[0][1];

        if (kind === "enum") {
            // Handle enum values
            const enumValues: string[] = Object.entries(details)[1][1];

            enumMigrations.addEnumValues(tableNameCamel, Object.keys(enumValues));
            tableIncludes.push(`- "!include public_enum_${tableNameSnake}.yaml"`);
        } else if (kind === "entity") {
            const properties = Object.entries(details)[1][1] as EntityProperties;
            const metadata = generateMetadataForTable(tableNameSnake, properties);

            writeMetadataToFile(tableNameSnake, metadata);
            tableIncludes.push(`- "!include public_${tableNameSnake}.yaml"`);

            for (const [propertyName, property] of Object.entries(properties)) {
                if (property.type.kind === "enum") {
                    const enumName = property.type.name;
                    const enumNameSnake = camelToSnakeCase(enumName);
                    const enumMetadata = generateEnumMetadata(enumNameSnake);
                    writeMetadataToFile(`enum_${enumNameSnake}`, enumMetadata);

                    enumMigrations.addTableColumnMapping(enumName, tableNameSnake, propertyName);
                }
            }
        }
    }

    // Include migrations table
    tableIncludes.push(`- "!include public_migrations.yaml"`);

    // Write tables.yaml with includes
    fs.writeFileSync(`${tablesDirPath}/tables.yaml`, tableIncludes.join("\n"));

    // Generate enum migrations
    enumMigrations.generateMigrationFiles();

    // Generate metadata for custom functions
    generateFunctions(['get_ud_history']);

    // Geneate static metadata files
    generateStaticConfig();
}

/**
 * Generates relationships from the properties of an entity.
 */
function getRelationships(properties: EntityProperties) {
    const objectRelationships: (ObjectRelationship | LookupRelationship)[] = [];
    const arrayRelationships: ArrayRelationship[] = [];

    for (const [relationName, relation] of Object.entries(properties)) {
        const relation_name = camelToSnakeCase(relationName);
        if (relation.type.kind === 'fk') {
            objectRelationships.push({
                name: relationName,
                using: {
                    foreign_key_constraint_on: relation_name + "_id",
                },
            });
        } else if (relation.type.kind === 'list-lookup' || relation.type.kind === 'lookup') {
            const entity = camelToSnakeCase(relation.type.entity);
            const snake_field = camelToSnakeCase(relation.type.field);
            const relationship = {
                name: relationName,
                using: {
                    foreign_key_constraint_on: {
                        column: snake_field + "_id",
                        table: {
                            name: entity,
                            schema: "public",
                        },
                    },
                },
            };
            if (relation.type.kind === 'list-lookup') {
                arrayRelationships.push(relationship as ArrayRelationship);
            } else {
                objectRelationships.push(relationship);
            }
        }
    }
    return { objectRelationships, arrayRelationships };
}

/**
 * Generates metadata for a table based on its properties and relationships.
 */
function generateMetadataForTable(tableName: string, properties: EntityProperties): HasuraTable {
    const { objectRelationships, arrayRelationships } = getRelationships(properties);
    let tableMetadata: HasuraTable = {
        table: {
            name: tableName,
            schema: "public",
        },
        object_relationships: objectRelationships,
        array_relationships: arrayRelationships,
        select_permissions: [{
            role: "public",
            permission: {
                columns: "*",
                filter: {},
                allow_aggregations: true,
            },
        }],
    };

    if (tableName === "identity") {
        tableMetadata = addUdHistoryComputedFields(tableMetadata);
    }

    return tableMetadata;
}

/**
 * Adds computed fields for the 'identity' table, if applicable.
 */
function addUdHistoryComputedFields(tableMetadata: HasuraTable) {
    const udHistoryIndex = tableMetadata.array_relationships.findIndex((relationship) => relationship.name === "ud_history");
    tableMetadata.array_relationships.splice(udHistoryIndex, 1);

    tableMetadata.computed_fields = [{
        name: "udHistory",
        definition: {
            function: {
                name: "get_ud_history",
                schema: "public",
            },
        },
        comment: '"Get UD History by Identity"',
    }];

    return tableMetadata;
}

/**
 * Generates metadata for custom functions and writes them to YAML files.
 */
function generateFunctions(functionNames: string[]) {
    const dirPath = `${metadataDbPath}/functions`;
    ensureDirectoryExists(dirPath);

    const functionsYAMLContent: string[] = functionNames.map(name => {
        const functionMetadata = {
            function: {
                name,
                schema: 'public',
            },
        };
        const yamlContent = yaml.dump(functionMetadata);
        fs.writeFileSync(`${dirPath}/public_${name}.yaml`, yamlContent);
        return `- "!include public_${name}.yaml"`;
    });

    fs.writeFileSync(`${dirPath}/functions.yaml`, functionsYAMLContent.join("\n"));
}

/**
 * Generates metadata for an enum type.
 */
function generateEnumMetadata(enumName: string): Object {
    return {
        table: {
            schema: "public",
            name: enumName
        },
        is_enum: true
    };
}

/**
 * Writes metadata to a YAML file for a given table or function.
 */
function writeMetadataToFile(typeName: string, metadata: Object) {
    const yamlContent = yaml.dump(metadata);
    const dirPath = `${metadataDbPath}/tables`;
    fs.writeFileSync(`${dirPath}/public_${typeName}.yaml`, yamlContent);
}

/**
 * Writes the static metadata files, including databases.yaml and version.yaml.
 */
function generateStaticConfig() {
    const databasesYAMLContent = yaml.dump([{
        name: "default",
        kind: "postgres",
        configuration: {
            connection_info: {
                database_url: {
                    from_env: "HASURA_GRAPHQL_DATABASE_URL"
                },
                isolation_level: "read-committed",
                pool_settings: {
                    connection_lifetime: 600,
                    idle_timeout: 180,
                    max_connections: 50,
                    retries: 1
                },
                use_prepared_statements: true
            },
        },
        customization: {
            naming_convention: "graphql-default"
        },
        tables: "!include default/tables/tables.yaml",
        functions: "!include default/functions/functions.yaml"
    }]);

    const migrationsYAMLContent = yaml.dump({
        table: {
            name: "migrations",
            schema: "public"
        }
    });

    fs.writeFileSync(`${metadataPath}/databases/databases.yaml`, databasesYAMLContent);
    fs.writeFileSync(`${metadataDbPath}/tables/public_migrations.yaml`, migrationsYAMLContent);
    fs.writeFileSync(`${metadataPath}/version.yaml`, "version: 3\n");
}

generateMetadata();
