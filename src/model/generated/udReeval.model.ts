import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, ManyToOne as ManyToOne_, Index as Index_, DateTimeColumn as DateTimeColumn_, BigIntColumn as BigIntColumn_} from "@subsquid/typeorm-store"
import {Event} from "./event.model"

/**
 * List of reevaluation of Universal Dividend based on changes in monetary mass and number of members. Every 6 months in Ğ1
 */
@Entity_()
export class UdReeval {
    constructor(props?: Partial<UdReeval>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @IntColumn_({nullable: false})
    blockNumber!: number

    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    @DateTimeColumn_({nullable: false})
    timestamp!: Date

    @BigIntColumn_({nullable: false})
    newUdAmount!: bigint

    @BigIntColumn_({nullable: false})
    monetaryMass!: bigint

    @IntColumn_({nullable: false})
    membersCount!: number
}
