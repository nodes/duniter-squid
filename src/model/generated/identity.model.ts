import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, Index as Index_, OneToOne as OneToOne_, JoinColumn as JoinColumn_, ManyToOne as ManyToOne_, StringColumn as StringColumn_, OneToMany as OneToMany_, BooleanColumn as BooleanColumn_} from "@subsquid/typeorm-store"
import {Account} from "./account.model"
import {IdentityStatus} from "./_identityStatus"
import {Event} from "./event.model"
import {Cert} from "./cert.model"
import {MembershipEvent} from "./membershipEvent.model"
import {ChangeOwnerKey} from "./changeOwnerKey.model"
import {Smith} from "./smith.model"
import {UdHistory} from "./udHistory.model"

/**
 * Identity
 */
@Entity_()
export class Identity {
    constructor(props?: Partial<Identity>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * Identity index
     */
    @Index_({unique: true})
    @IntColumn_({nullable: false})
    index!: number

    /**
     * Current account
     */
    @Index_({unique: true})
    @OneToOne_(() => Account, {nullable: true})
    @JoinColumn_()
    account!: Account | undefined | null

    /**
     * Let track the account in case identity was removed
     */
    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    accountRemoved!: Account | undefined | null

    /**
     * Name
     */
    @Index_()
    @StringColumn_({nullable: false})
    name!: string

    /**
     * Status of the identity
     */
    @Index_()
    @Column_("varchar", {length: 11, nullable: false})
    status!: IdentityStatus

    /**
     * Block number of identity creation event
     */
    @IntColumn_({nullable: false})
    createdOn!: number

    /**
     * Event corresponding of identity creation event
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    createdIn!: Event

    /**
     * Block number of last identity, changeOwnerKey and membership active event
     */
    @IntColumn_({nullable: false})
    lastChangeOn!: number

    /**
     * Certifications issued
     */
    @OneToMany_(() => Cert, e => e.issuer)
    certIssued!: Cert[]

    /**
     * Certifications received
     */
    @OneToMany_(() => Cert, e => e.receiver)
    certReceived!: Cert[]

    /**
     * True if the identity is a member
     */
    @BooleanColumn_({nullable: false})
    isMember!: boolean

    /**
     * the current expireOn value
     */
    @IntColumn_({nullable: false})
    expireOn!: number

    /**
     * history of the membership changes events
     */
    @OneToMany_(() => MembershipEvent, e => e.identity)
    membershipHistory!: MembershipEvent[]

    /**
     * Owner key changes
     */
    @OneToMany_(() => ChangeOwnerKey, e => e.identity)
    ownerKeyChange!: ChangeOwnerKey[]

    /**
     * linked accounts
     */
    @OneToMany_(() => Account, e => e.linkedIdentity)
    linkedAccount!: Account[]

    /**
     * Smith information
     */
    @OneToOne_(() => Smith, e => e.identity)
    smith!: Smith | undefined | null

    /**
     * Universal Dividend history
     */
    @OneToMany_(() => UdHistory, e => e.identity)
    udHistory!: UdHistory[]
}
