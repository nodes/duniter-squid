import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, Index as Index_} from "@subsquid/typeorm-store"

/**
 * History of the blockchain population.
 */
@Entity_()
export class PopulationHistory {
    constructor(props?: Partial<PopulationHistory>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * The count of smiths at this point in the history.
     */
    @IntColumn_({nullable: false})
    smithCount!: number

    /**
     * The count of members at this point in the history.
     */
    @IntColumn_({nullable: false})
    memberCount!: number

    /**
     * The count of active accounts at this point in the history.
     */
    @IntColumn_({nullable: false})
    activeAccountCount!: number

    /**
     * The block number at which this history record was created.
     */
    @Index_({unique: true})
    @IntColumn_({nullable: false})
    blockNumber!: number
}
