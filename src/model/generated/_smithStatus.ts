export enum SmithStatus {
    Invited = "Invited",
    Pending = "Pending",
    Smith = "Smith",
    Excluded = "Excluded",
}
