import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, BooleanColumn as BooleanColumn_, IntColumn as IntColumn_, OneToMany as OneToMany_} from "@subsquid/typeorm-store"
import {Identity} from "./identity.model"
import {Event} from "./event.model"
import {CertEvent} from "./certEvent.model"

/**
 * Certification
 */
@Entity_()
export class Cert {
    constructor(props?: Partial<Cert>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * certification source
     */
    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    issuer!: Identity

    /**
     * certification target
     */
    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    receiver!: Identity

    /**
     * whether the certification is currently active or not
     */
    @BooleanColumn_({nullable: false})
    isActive!: boolean

    /**
     * the first block number of the certification creation
     */
    @IntColumn_({nullable: false})
    createdOn!: number

    /**
     * the event corresponding to the first certification creation
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    createdIn!: Event

    /**
     * the last block number of the certification renewal
     */
    @IntColumn_({nullable: false})
    updatedOn!: number

    /**
     * the event corresponding to the last certification renewal
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    updatedIn!: Event

    /**
     * the current expireOn value
     */
    @IntColumn_({nullable: false})
    expireOn!: number

    /**
     * list all events of this cert
     */
    @OneToMany_(() => CertEvent, e => e.cert)
    certHistory!: CertEvent[]
}
