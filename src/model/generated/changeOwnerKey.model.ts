import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, IntColumn as IntColumn_} from "@subsquid/typeorm-store"
import {Identity} from "./identity.model"
import {Account} from "./account.model"

/**
 * owner key change
 */
@Entity_()
export class ChangeOwnerKey {
    constructor(props?: Partial<ChangeOwnerKey>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    identity!: Identity

    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    previous!: Account

    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    next!: Account

    @IntColumn_({nullable: false})
    blockNumber!: number
}
