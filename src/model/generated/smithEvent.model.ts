import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, IntColumn as IntColumn_} from "@subsquid/typeorm-store"
import {Smith} from "./smith.model"
import {SmithEventType} from "./_smithEventType"
import {Event} from "./event.model"

@Entity_()
export class SmithEvent {
    constructor(props?: Partial<SmithEvent>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Smith, {nullable: true})
    smith!: Smith

    @Column_("varchar", {length: 8, nullable: false})
    eventType!: SmithEventType

    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    @Index_()
    @IntColumn_({nullable: false})
    blockNumber!: number
}
