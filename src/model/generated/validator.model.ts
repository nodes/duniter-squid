import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, Index as Index_} from "@subsquid/typeorm-store"

/**
 * Since identities can change owner key, validator table helps track which smith is forging the block
 */
@Entity_()
export class Validator {
    constructor(props?: Partial<Validator>) {
        Object.assign(this, props)
    }

    /**
     * SS58 of pubkey used at least once to compute a block
     */
    @PrimaryColumn_()
    id!: string

    /**
     * Identity index of Smith who owned this pubkey
     */
    @Index_()
    @IntColumn_({nullable: false})
    index!: number
}
