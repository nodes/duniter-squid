export enum EventType {
    Creation = "Creation",
    Renewal = "Renewal",
    Removal = "Removal",
}
