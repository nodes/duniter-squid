import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, ManyToOne as ManyToOne_, Index as Index_, BytesColumn as BytesColumn_, StringColumn as StringColumn_} from "@subsquid/typeorm-store"
import {Account} from "./account.model"
import {Event} from "./event.model"
import {CommentType} from "./_commentType"

/**
 * transaction comment
 */
@Entity_()
export class TxComment {
    constructor(props?: Partial<TxComment>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    /**
     * Block number of the comment
     */
    @IntColumn_({nullable: false})
    blockNumber!: number

    /**
     * Author of the comment
     */
    @Index_()
    @ManyToOne_(() => Account, {nullable: true})
    author!: Account

    /**
     * Event where the comment comes from
     */
    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    /**
     * Raw remark as present on the blockchain
     */
    @BytesColumn_({nullable: false})
    remarkBytes!: Uint8Array

    /**
     * Remark decoded as string
     */
    @StringColumn_({nullable: false})
    remark!: string

    /**
     * Blake two 256 hash published by the blockchain in the remark event
     */
    @StringColumn_({nullable: false})
    hash!: string

    /**
     * Type of the comment
     */
    @Column_("varchar", {length: 7, nullable: false})
    type!: CommentType
}
