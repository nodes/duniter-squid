export enum IdentityStatus {
    Unconfirmed = "Unconfirmed",
    Unvalidated = "Unvalidated",
    Member = "Member",
    NotMember = "NotMember",
    Revoked = "Revoked",
    Removed = "Removed",
}
