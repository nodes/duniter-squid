import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, IntColumn as IntColumn_, OneToOne as OneToOne_, OneToMany as OneToMany_, ManyToOne as ManyToOne_, Index as Index_, BooleanColumn as BooleanColumn_} from "@subsquid/typeorm-store"
import {Identity} from "./identity.model"
import {ChangeOwnerKey} from "./changeOwnerKey.model"
import {Transfer} from "./transfer.model"
import {TxComment} from "./txComment.model"

/**
 * Account table identified by its ss58 address
 */
@Entity_()
export class Account {
    constructor(props?: Partial<Account>) {
        Object.assign(this, props)
    }

    /**
     * Account address is SS58 format
     */
    @PrimaryColumn_()
    id!: string

    /**
     * Block number of account creation
     */
    @IntColumn_({nullable: false})
    createdOn!: number

    /**
     * current account for the identity
     */
    @OneToOne_(() => Identity, e => e.account)
    identity!: Identity | undefined | null

    /**
     * removed identities on this account
     */
    @OneToMany_(() => Identity, e => e.accountRemoved)
    removedIdentities!: Identity[]

    /**
     * was once account of the identity
     */
    @OneToMany_(() => ChangeOwnerKey, e => e.previous)
    wasIdentity!: ChangeOwnerKey[]

    /**
     * linked to the identity
     */
    @Index_()
    @ManyToOne_(() => Identity, {nullable: true})
    linkedIdentity!: Identity | undefined | null

    /**
     * transfers issued by this account
     */
    @OneToMany_(() => Transfer, e => e.from)
    transfersIssued!: Transfer[]

    /**
     * transfers received by this account
     */
    @OneToMany_(() => Transfer, e => e.to)
    transfersReceived!: Transfer[]

    /**
     * comments issued
     */
    @OneToMany_(() => TxComment, e => e.author)
    commentsIssued!: TxComment[]

    /**
     * is currently active
     */
    @BooleanColumn_({nullable: false})
    isActive!: boolean
}
