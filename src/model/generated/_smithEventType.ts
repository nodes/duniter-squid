export enum SmithEventType {
    Invited = "Invited",
    Accepted = "Accepted",
    Promoted = "Promoted",
    Excluded = "Excluded",
}
