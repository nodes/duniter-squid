import {Entity as Entity_, Column as Column_, PrimaryColumn as PrimaryColumn_, ManyToOne as ManyToOne_, Index as Index_, IntColumn as IntColumn_} from "@subsquid/typeorm-store"
import {Cert} from "./cert.model"
import {Event} from "./event.model"
import {EventType} from "./_eventType"

/**
 * Certification event
 */
@Entity_()
export class CertEvent {
    constructor(props?: Partial<CertEvent>) {
        Object.assign(this, props)
    }

    @PrimaryColumn_()
    id!: string

    @Index_()
    @ManyToOne_(() => Cert, {nullable: true})
    cert!: Cert

    @Index_()
    @ManyToOne_(() => Event, {nullable: true})
    event!: Event

    @Index_()
    @IntColumn_({nullable: false})
    blockNumber!: number

    @Column_("varchar", {length: 8, nullable: false})
    eventType!: EventType
}
