export enum CommentType {
    Cid = "Cid",
    Ascii = "Ascii",
    Unicode = "Unicode",
    Raw = "Raw",
}
