import {
    BlockHeader,
    DataHandlerContext,
    SubstrateBatchProcessor,
    SubstrateBatchProcessorFields,
    Call as _Call,
    Event as _Event,
    Extrinsic as _Extrinsic
} from '@subsquid/substrate-processor'
import { assertNotNull } from '@subsquid/util-internal'

export const processor = new SubstrateBatchProcessor()
    .setRpcEndpoint(
        // Lookup archive by the network name in Subsquid registry
        // See https://docs.subsquid.io/substrate-indexing/supported-networks/
        // archive: lookupArchive('gdev', {genesis: '0xc2347f3d89dc190608abe2e09311c10dc1b3fd9555935ef994ac93c5f279a857'}),
        // Chain RPC endpoint is required on Substrate for metadata and real-time updates
        {
            // Set via .env for local runs or via secrets when deploying to Subsquid Cloud
            // https://docs.subsquid.io/deploy-squid/env-variables/
            url: assertNotNull(process.env.RPC_ENDPOINT),
            // More RPC connection options at https://docs.subsquid.io/substrate-indexing/setup/general/#set-data-source
            // rateLimit: 10
        })
    .addEvent({
        // name: [],
        // name: [events.balances.transfer.name, events.cert.newCert.name, events.cert.renewedCert.name, events.cert.removedCert.name],
        // extrinsic: true,
        // https://github.com/subsquid-labs/giant-squid-explorer/blob/main/src/processor.ts
        call: true,
        extrinsic: true,
    })
    .addCall({
        // name: [],
        // events: true,
        // https://github.com/subsquid-labs/giant-squid-explorer/blob/main/src/processor.ts
        extrinsic: true,
        stack: true,
    })
    .setFields({
        // this part of the processor config comes from giant squid
        // https://github.com/subsquid-labs/giant-squid-explorer/blob/main/src/processor.ts
        block: {
            timestamp: true,
            digest: true,
            extrinsicsRoot: true,
            stateRoot: true,
            validator: true,
        },
        call: {
            name: true,
            args: true,
            origin: true,
            success: true,
            error: true,
        },
        event: {
            name: true,
            args: true,
            phase: true,
        },
        extrinsic: {
            hash: true,
            success: true,
            error: true,
            fee: true,
            signature: true,
            tip: true,
            version: true,
        },
    })
// Uncomment to disable RPC ingestion and drastically reduce no of RPC calls
//.useArchiveOnly()

export type Fields = SubstrateBatchProcessorFields<typeof processor>
export type Block = BlockHeader<Fields>
export type Event = _Event<Fields>
export type Call = _Call<Fields>
export type Extrinsic = _Extrinsic<Fields>
export type ProcessorContext<Store> = DataHandlerContext<Store, Fields>
