import { readFileSync } from "fs";
import path from "path/posix";
import { Account, Block, Cert, CertEvent, CommentType, Event, EventType, Smith, PopulationHistory, Identity, MembershipEvent, SmithEvent, SmithEventType, SmithCert, SmithStatus, Transfer, Validator, TxComment } from "../model";
import type { Address, BlockV1, Certv1, Ctx, Genesis, Genv1, IdtyIndex, TransactionHistory } from "../types_custom";
import { bytesToString, hexToUint8Array, safePubkeyToAddress, v1_to_v2_height } from "../utils";
import { AccountId32 } from "../types/v800";
import bs58 from "bs58";

export async function saveGenesis(ctx: Ctx, block: Block) {
  const genesis_path = process.env.GENESIS_FILE!
  const hist_gen_path = process.env.HIST_GEN_FILE!
  const hist_block_path = process.env.HIST_BLOCK_FILE!
  const hist_tx_path = process.env.HIST_TX_FILE!
  const hist_cert_path = process.env.HIST_CERT_FILE!

  const gen: Genv1 = JSON.parse(readFileSync(path.resolve(hist_gen_path)).toString())

  // v1 to v2 block number conversion 
  const last_v1_block = gen.current_block.number
  const v1_to_v2 = (n: number) => v1_to_v2_height(n, last_v1_block) // converter
  const NOTIF_INTERVAL = 100000
  // cert validity in number of v2 blocks
  // const CERTVALIDITYv2 = 10519200 // 2 years in blocks = 2 * 365.25 * 24 * 60 * 60 / 6
  // cert validity in number of v1 blocks
  // time interval of v1 blocks measured between block 0 and 729313
  // 311.17641533 seconds unix2datetime(1715932132) - unix2datetime(1488987127) / 729313
  const CERTVALIDITYv1 = 202828 // 2 years in blocks = 2 * 365.25 * 24 * 60 * 60 / 311.17641533
  // with 5 min block time (300 seconds) validity would be 105192

  // negative block number to history blocks
  const blocks: Map<number, Block> = new Map()
  const certEvents: CertEvent[] = [];
  const fakeBlockEvents: Map<number, Event> = new Map()

  /// create a fake event for give block
  /// blockNumber: positive v1 block number
  /// block: reference to block
  /// negHeight: negative height computed from blockNumber 
  function createFakeEvent(blockNumber: number, block: Block, negHeight: number): Event {
    // create fake event for this block
    const fakeEvent = new Event({
      id: `genesis-event_v1-${blockNumber}`,
      block: block,
      index: 0,
      phase: 'genesis-phase',
      pallet: 'genesis-pallet',
      name: 'genesis-name',
    })
    fakeBlockEvents.set(negHeight, fakeEvent)
    // make sure the block has event
    block.eventsCount = 1
    return fakeEvent
  }

  // ===========================================
  // === v1 block history ===

  ctx.log.info("Handling v1 block history");

  const blocksv1: Array<BlockV1> = JSON.parse(readFileSync(path.resolve(hist_block_path)).toString())
  for (const b of blocksv1) {
    // only older blocks
    if (b.height > last_v1_block) continue
    // height conversion
    const negHeight = v1_to_v2(b.height)
    if (negHeight % NOTIF_INTERVAL == 0) ctx.log.info("" + negHeight)
    // create block
    const negBlock = new Block({
      id: `genesis-block_v1-${b.height}`,
      height: negHeight,
      timestamp: new Date(b.timestamp * 1000),
      hash: hexToUint8Array(b.hash),
      parentHash: b.parentHash ? hexToUint8Array(b.parentHash) : block.parentHash,
      validator: bs58.decode(b.validator),
      eventsCount: b.hasEvent ? 1 : 0, // note: overwritten by createFakeEvent just after
      stateRoot: new Uint8Array(),
      extrinsicsicRoot: new Uint8Array(),
      specVersion: b.version,
      extrinsicsCount: 0,
      implVersion: b.version,
      specName: "",
      implName: "",
      callsCount: 0,
    });
    blocks.set(negHeight, negBlock)
    // no need to create fake event if block is empty
    if (!b.hasEvent) continue
    // create fake event for this block
    createFakeEvent(b.height, negBlock, negHeight)
  }

  // create fake genesis event to mark identities as created in this block
  // TODO with membership history we can use negative block instead
  const genesis_event = new Event({
    id: "genesis-event_0",
    index: 0,
    block,
    phase: 'genesis-phase',
    pallet: 'genesis-pallet',
    name: 'genesis-name',
  });
  fakeBlockEvents.set(0, genesis_event)

  // SAVE v1 block history
  ctx.log.info("Saving v1 block history");
  await ctx.store.insert([...blocks.values()]);
  await ctx.store.insert([...fakeBlockEvents.values()]);

  // ===========================================
  // === genesis file ===

  ctx.log.info("Loading genesis file");

  // Read genesis json
  const genesisData = JSON.parse(readFileSync(path.resolve(process.cwd(), genesis_path)).toString());
  // Accomodate for versions of polkadot before and after 1.6.0:
  const genesis: Genesis = genesisData.genesis.runtimeAndCode?.runtime || genesisData.genesis.runtimeGenesis.patch;

  const accounts: Map<Address, Account> = new Map();
  const identities: Map<IdtyIndex, Identity> = new Map();
  const validators: Map<Address, Validator> = new Map();
  const smiths: Map<IdtyIndex, Smith> = new Map();
  const identitiesMap: Map<AccountId32, IdtyIndex> = new Map();
  const certs: Map<string, Cert> = new Map();
  const smithCerts: SmithCert[] = [];
  const membershipsEvents: MembershipEvent[] = [];
  const smithsEvents: SmithEvent[] = [];
  const populationHistory = new PopulationHistory({ id: "genesis-population", blockNumber: -1, activeAccountCount: 0, memberCount: 0, smithCount: 0 })

  // --- collect accounts ---
  for (const [address, value] of Object.entries(genesis.account.accounts)) {
    // Accounts with 0 will not be added in blockchain.
    // Accounts with less than 1ED will be reaped at first block.
    const isActive = value.balance > 0;
    accounts.set(
      address,
      new Account({
        id: address,
        isActive,
        // init to null and update later to handle circular dependency
        linkedIdentity: null,
        createdOn: 0 // TODO track block creation of v1 account
      })
    );
    if (isActive) {
      populationHistory.activeAccountCount += 1;
    }
  }

  // --- collect identities ---
  for (const idty of genesis.identity.identities) {
    const account = accounts.get(idty.value.owner_key);
    const the_identity = new Identity({
      id: `genesis-identity_${idty.index}`,
      index: idty.index,
      account,
      name: bytesToString(idty.name),
      status: idty.value.status,
      isMember: false,
      lastChangeOn: 0,
      createdOn: 0,
      createdIn: genesis_event,
      expireOn: 0,
    });

    // add identity to list
    identities.set(idty.index, the_identity);
    identitiesMap.set(idty.value.owner_key, idty.index);
  }

  // --- collect memberships ---
  for (const [idtyIndex, mshipInfo] of Object.entries(genesis.membership.memberships)) {
    const identity = identities.get(parseInt(idtyIndex))!;
    identity.isMember = true;
    identity.expireOn = mshipInfo.expire_on;
    membershipsEvents.push(
      new MembershipEvent({
        id: `genesis-membership_${idtyIndex}`,
        identity: identity,
        eventType: EventType.Creation,
        event: genesis_event,
        blockNumber: 0,
      })
    );
    populationHistory.memberCount += 1;
  }

  // --- collect certifications ---
  for (const [receiver_index, certs_received] of Object.entries(genesis.certification.certsByReceiver)) {
    for (const [issuer_index, expiration_block] of Object.entries(certs_received)) {
      const certstr = `${issuer_index}-${receiver_index}`
      certs.set(certstr,
        new Cert({
          // cert id is different in genesis than in the rest of blockchain
          id: `genesis-cert_${certstr}`,
          isActive: true,
          issuer: identities.get(parseInt(issuer_index)),
          receiver: identities.get(parseInt(receiver_index)),
          createdOn: 0,
          updatedOn: 0,
          createdIn: genesis_event,
          updatedIn: genesis_event,
          expireOn: expiration_block as number,
          certHistory: [],
        })
      );
    }
  }

  // --- collect genesis smith ---
  // loop once to add smith entities
  for (const [idtyIndex, _] of Object.entries(genesis.smithMembers.initialSmiths)) {
    const idtyIdx = parseInt(idtyIndex)
    const identity = identities.get(idtyIdx)!;

    const smith = new Smith({
      id: `genesis-smith_${idtyIndex}`,
      index: idtyIdx,
      identity: identity,
      forged: 0,
      lastChanged: 0,
      smithStatus: SmithStatus.Smith,
    });
    smiths.set(idtyIdx, smith);
    populationHistory.smithCount += 1;

    // smith must be non-removed identity with a non-null account
    const validator = new Validator({ id: identity.account!.id, index: idtyIdx })
    validators.set(validator.id, validator);

    smithsEvents.push(
      new SmithEvent({
        id: `genesis-smith_${idtyIndex}`,
        smith: smiths.get(idtyIdx)!,
        eventType: SmithEventType.Promoted,
        event: genesis_event,
        blockNumber: 0,
      })
    );
  }
  // loop another time to add smith certs
  for (const [idtyIndex, smithCertsData] of Object.entries(genesis.smithMembers.initialSmiths)) {
    const idtyIdx = parseInt(idtyIndex)
    const [_, certs] = smithCertsData as [boolean, number[]];
    for (const issuer_index of certs) {
      smithCerts.push(
        new SmithCert({
          id: `genesis-smith-membership_${issuer_index}-${idtyIndex}`,
          issuer: smiths.get(issuer_index)!,
          receiver: smiths.get(idtyIdx)!,
          createdOn: 0,
        }),
      );
    }
  }

  // SAVE genesis data
  ctx.log.info("Saving data from genesis file");

  // insert everything in storage
  await ctx.store.insert([...accounts.values()]);
  await ctx.store.insert([...identities.values()]);
  await ctx.store.insert([...certs.values()]);

  // HACK to handle circular dependency, linkedIdentity should be
  // updated after being created null AND inserted in db
  for (const [address, value] of Object.entries(genesis.account.accounts)) {
    let account = accounts.get(address);
    let identity = identities.get(parseInt(value.idty_id));
    if (account && identity) {
      account.linkedIdentity = identity;
      accounts.set(address, account);
    }
  }
  // accounts are marked for update with new linked identity
  await ctx.store.upsert([...accounts.values()]);
  await ctx.store.insert([...smiths.values()]);
  await ctx.store.insert([...validators.values()]);
  // membership events only includes genesis related events
  // TODO also include v1 membership history
  // https://git.duniter.org/tools/py-g1-migrator/-/issues/5
  await ctx.store.insert(membershipsEvents);
  await ctx.store.insert(smithsEvents);
  await ctx.store.insert(smithCerts);
  await ctx.store.insert(populationHistory);

  // ===========================================
  // === v1 cert history (after genesis file) ===

  ctx.log.info("Process cert history");

  const cert_hist: Array<Certv1> = JSON.parse(readFileSync(path.resolve(hist_cert_path)).toString())
  for (const c of cert_hist) {
    // only older blocks
    if (c.blockNumber > last_v1_block) continue
    // height conversion
    const negHeight = v1_to_v2(c.blockNumber)
    const negExpire = negHeight + CERTVALIDITYv1
    // process cert
    const certstr = `${c.issuer}-${c.receiver}`
    let cert = certs.get(certstr)
    if (cert == undefined) {
      // if cert expiry is positive, certification must have been present in genesis
      if (negExpire > 0) {
        console.log("cert not present in genesis but supposed to be expired after");
        console.log(c);
        console.log(negHeight);
        console.log(negExpire);
      }
      // create cert
      cert = new Cert({
        id: `genesis-cert_${certstr}`,
        isActive: false, // we know this cert is not active at genesis
        issuer: identities.get(c.issuer)!,
        receiver: identities.get(c.receiver)!,
        createdOn: negHeight,
        createdIn: fakeBlockEvents.get(negHeight)!,
        updatedOn: negHeight,
        updatedIn: fakeBlockEvents.get(negHeight)!,
        expireOn: negExpire,
      });
      certs.set(certstr, cert)
    } else {
      switch (c.type) {
        case EventType.Creation:
          // TODO decide what should be the value of createdOn
          // first creation or last creation ?
          if (cert.createdOn == 0) {
            cert.createdOn = negHeight
            cert.createdIn = fakeBlockEvents.get(negHeight)!
          }
          // update cert in any case
          cert.updatedOn = negHeight
          cert.updatedIn = fakeBlockEvents.get(negHeight)!
          break
        case EventType.Renewal:
          // only update expiration block if it is before genesis
          // otherwise, it will be known at genesis
          if (negExpire < 0) {
            cert.expireOn = negExpire
          }
          // update cert
          cert.updatedOn = negHeight
          cert.updatedIn = fakeBlockEvents.get(negHeight)!
          break
        case EventType.Removal:
          // do not touch isActive since the genesis value is already known
          // cert removal does not touch cert "updatedOn"
          break
      }
    }
    let event = fakeBlockEvents.get(negHeight)
    if (event == undefined) {
      // this can happen for cert removal because duniter thinks that hasEvent = false
      event = createFakeEvent(c.blockNumber, blocks.get(negHeight)!, negHeight)
    }
    // add new cert event to trace this history
    const certEvent = new CertEvent({
      id: `genesis-cert-event_${c.issuer}-${c.receiver}_v1-${c.blockNumber}`,
      blockNumber: negHeight,
      eventType: c.type as EventType,
      cert,
      event,
    });
    certEvents.push(certEvent);
  }

  // SAVE cert events
  ctx.log.info("Saving certification history");
  // add the fake events needed for these certs
  await ctx.store.upsert([...fakeBlockEvents.values()]);
  // add the certs needed for the history and not present in the genesis
  await ctx.store.upsert([...certs.values()]);
  await ctx.store.insert(certEvents);

  // ===========================================

  // accounts present in transaction history but not in genesis
  const genesis_transfers: Transfer[] = [];
  const genesis_txcomments: TxComment[] = [];
  let unknown_wallet_count = 0;

  // Read transaction history json
  ctx.log.info("Loading transaction history");
  const tx_history: TransactionHistory = JSON.parse(readFileSync(path.resolve(hist_tx_path)).toString())

  // make sure that all tx source and dest accounts exist so that we can get them later when adding txs
  // we know that these accounts are not active anymore (otherwise they would have been added before)
  for (const tx of tx_history) {
    const issuer = safePubkeyToAddress(tx.from)
    const receiver = safePubkeyToAddress(tx.to)
    if (!accounts.has(issuer)) {
      unknown_wallet_count++
      accounts.set(issuer, new Account({
        id: issuer,
        isActive: false,
        createdOn: 0 // TODO track block creation of v1 account
      }));
    }
    if (!accounts.has(receiver)) {
      unknown_wallet_count++
      accounts.set(receiver, new Account({
        id: receiver,
        isActive: false,
        createdOn: 0 // TODO track block creation of v1 account
      }));
    }
  }
  ctx.log.info(`There are ${accounts.size} wallets from genesis and ${unknown_wallet_count} additional wallets from tx history`);
  ctx.log.info(`   (total ${accounts.size + unknown_wallet_count})`);

  // add txs
  let genesis_tx_counter = 0;
  for (const tx of tx_history) {
    // only older blocks
    if (tx.blockNumber > last_v1_block) continue
    // height conversion
    const negHeight = v1_to_v2(tx.blockNumber)
    // process
    genesis_tx_counter += 1;

    let event = fakeBlockEvents.get(negHeight)
    // with real data this event should not be null

    const date = new Date(tx.timestamp * 1000); // seconds to milliseconds
    const from = accounts.get(safePubkeyToAddress(tx.from));
    const to = accounts.get(safePubkeyToAddress(tx.to));
    const transfer = new Transfer({
      id: `genesis-tx_n°${genesis_tx_counter}`,
      blockNumber: negHeight,
      timestamp: date,
      from,
      to,
      amount: BigInt(tx.amount),
    })

    const txcomment = tx.comment
    if (txcomment != '') {
      const comment = new TxComment({
        id: `genesis-comment_n°${genesis_tx_counter}`,
        blockNumber: negHeight,
        author: from,
        event: event,
        remark: txcomment,
        remarkBytes: new Uint8Array(Buffer.from(txcomment)),
        hash: "",
        type: CommentType.Ascii
      })
      transfer.comment = comment
      genesis_txcomments.push(comment)
    }
    genesis_transfers.push(transfer);
  }

  // SAVE
  ctx.log.info("Saving v1 transaction history and comments");
  // adding transfers and comments requires to add accounts unknown in the genesis
  await ctx.store.upsert([...accounts.values()]);
  await ctx.store.insert(genesis_txcomments);
  await ctx.store.insert(genesis_transfers);

  // FLUSH
  ctx.log.info("Flushing changes to storage, this can take a while...");
  ctx.log.info("(about ~5 minutes for all g1 history and genesis data)");
  await ctx.store.flush();
  ctx.log.info("Genesis flushed");

  // START
  ctx.log.info("=====================");
  ctx.log.info(`Starting blockchain indexing with ${populationHistory.smithCount} smiths, ${populationHistory.memberCount} members and ${populationHistory.activeAccountCount} accounts!`);
}
