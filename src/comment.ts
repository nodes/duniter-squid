// utils to parse transaction comments

import { CommentType } from "./model";
import { CID } from 'multiformats'


/// convert a raw remark to a string with a type hint
export function getCommentType(raw: Buffer): [CommentType, string] {
    try {
        const cid = CID.decode(raw)
        return [CommentType.Cid, cid.toString()]
    } catch { }
    // not a CID

    const str = raw.toString("utf8")
    // only contains ascii chars
    if (/^[\x20-\x7E]*$/.test(str)) {
        return [CommentType.Ascii, str]
    }
    // does not contain non-printables
    if (!/[\x00-\x1F\x7F\x80-\x9F]/.test(str)) {
        return [CommentType.Unicode, str]
    }

    return [CommentType.Raw, "<raw bytes>"]
}