import * as ss58 from "@subsquid/ss58";
import fs from 'fs';
import { Address, B58Pubkey } from "./types_custom";
import bs58 from 'bs58';

/**
 * Define ss58 encoding with custom prefix
 */
const SS58_PREFIX = 42;
const PUBKEY_LENGTH = 32
const MAX_PUBKEY_LENGTH = 33

/**
 * Transform a public key to an ss58 encoded address
 */
export function pubkeyToAddress(pubkey: B58Pubkey): Address {
  // decode base58 public key
  const decoded = bs58.decode(pubkey);
  // add prefix and checksum to get ss58 address
  return ss58.codec(SS58_PREFIX).encode(decoded);
}

// same as above, but also work with pubkey with invalid length
export function safePubkeyToAddress(pubkey: B58Pubkey): Address {
  // decode base58 public key
  const decoded = bs58.decode(pubkey);
  if (decoded.length > MAX_PUBKEY_LENGTH) {
    console.log('invalid pubkey ' + pubkey)
    return ss58.codec(SS58_PREFIX).encode(new Uint8Array(PUBKEY_LENGTH).fill(0))
  }
  if (decoded.length < PUBKEY_LENGTH) {
    const missing = new Uint8Array(PUBKEY_LENGTH - decoded.length).fill(0)
    const filled = Uint8Array.from([...missing, ...decoded])
    // add prefix and checksum to get ss58 address
    return ss58.codec(SS58_PREFIX).encode(filled);
  }
  // ok for 33
  return ss58.codec(SS58_PREFIX).encode(decoded);
}


// convert hexadecimal string to ss58 address
export function ss58encode(pubkey: string): Address {
  return ss58.codec(SS58_PREFIX).encode(pubkey);
}

/**
 * Transform hex string to utf8 string
 */
export function hexToString(hex: string): string {
  return Buffer.from(hex.slice(2), "hex").toString("utf8");
}

/**
 * Transform bytes to utf8 string
 */
export function bytesToString(bytes: Array<number>): string {
  return bytes.map(code => String.fromCharCode(code)).join('');
}

export function camelToSnakeCase(str: string): string {
  return str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`).replace(/^_/, '');
}

/**
 * Ensures the existence of a directory and creates it if it doesn't exist.
 */
export function ensureDirectoryExists(dirPath: string) {
  if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true });
  }
}

/**
 * Converts a hexadecimal string to a Uint8Array.
 */
export function hexToUint8Array(hex: string): Uint8Array {
  if (hex.length % 2 !== 0) {
    throw new Error("The hexadecimal string must have an even length.");
  }

  const numBytes = hex.length / 2;
  const uint8Array = new Uint8Array(numBytes);

  for (let i = 0; i < numBytes; i++) {
    uint8Array[i] = parseInt(hex.substring(i * 2, (i + 1) * 2), 16);
  }

  return uint8Array;
}


/// converts v1 block number to negative value
export function v1_to_v2_height(v1blocknumber: number, maxblock: number) {
  return v1blocknumber - maxblock - 1
}